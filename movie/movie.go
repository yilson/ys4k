package movie

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"ys4k_gin/common"
	"ys4k_gin/logger"
)

var (
	ylog *logger.Logger
)

func Init() {
	logger, err := logger.NewLogger(&logger.LoggerConf{
		Dir:          "../logs",
		Filename:     "ys4kMovie.log",
		DailyRolling: true,
		SizeRolling:  true,
		MaxFileSize:  50 * logger.MB,
		MaxFileCount: 10,
		Withprefix:   true,
	})

	if err != nil {
		fmt.Printf("NewLogger error: ", err)
		os.Exit(1)
	}
	ylog = logger

}

/*
电影信息
videoId: 		"fb0df034df48c4986f68b023b13f2d81",
videoType: 		"4k",
videoSource:	"http://oss4k.chainedbox.com/%E7%BB%A3%E6%98%A5%E5%88%802%E4%BF%AE%E7%BD%97%E6%88%98%E5%9C%BA.Brotherhood.of.Blades.II.The.Infernal.Battlefield.2017.4K.2160p.WEB-DL.X264.AAC-BT4K.mkv",
videoLable: 	"绣春刀2:修罗战场",
videoCanvasUrl: "http://oss4k.chainedbox.com/%E7%BB%A3%E6%98%A5%E5%88%802.jpg"
*/
type MovieInfo struct {
	VideoId        string `json:"videoId"`
	VideoType      string `json:"videoType"`
	VideoSource    string `json:"videoSource"`
	VideoLable     string `json:"videoLable"`
	VideoCanvasUrl string `json:"videoCanvasUrl"`
}

type MovieInfoResp struct {
	Result []MovieInfo `json:"result"`
	Code   int         `json:"code"`
}

func MovieInfos(c *gin.Context) {
	mis := make([]MovieInfo, 0, 1)
	mi := MovieInfo{
		VideoId:        "fb0df034df48c4986f68b023b13f2d81",
		VideoType:      "4k",
		VideoSource:    "http://yunshang.oss-cn-beijing.aliyuncs.com/demo/4k/mts-out/kingsman.mp4",
		VideoLable:     "王牌学院1",
		VideoCanvasUrl: "http://yunshang.oss-cn-beijing.aliyuncs.com/demo/cover/kingsman.png",
	}
	mis = append(mis, mi)
	mi = MovieInfo{
		VideoId:        "505fed3c221a1c4c3fa1d0bf3d60d422",
		VideoType:      "4k",
		VideoSource:    "http://yunshang.oss-cn-beijing.aliyuncs.com/demo/4k/mts-out/TotalRecall.mp4",
		VideoLable:     "全面回忆",
		VideoCanvasUrl: "http://yunshang.oss-cn-beijing.aliyuncs.com/demo/cover/totalrecall.png",
	}
	mis = append(mis, mi)
	mi = MovieInfo{
		VideoId:        "ddd95b037e7e52743b7447c23b209fda",
		VideoType:      "4k",
		VideoSource:    "http://yunshang.oss-cn-beijing.aliyuncs.com/demo/4k/mts-out/Salt.mp4",
		VideoLable:     "特工绍特",
		VideoCanvasUrl: "http://yunshang.oss-cn-beijing.aliyuncs.com/demo/cover/salt.png",
	}
	mis = append(mis, mi)
	mi = MovieInfo{
		VideoId:        "b3a41efb83f9b176a04d1c33e5be28f8",
		VideoType:      "4k",
		VideoSource:    "http://yunshang.oss-cn-beijing.aliyuncs.com/demo/4k/mts-out/MadMax-FuryRoad.mp4",
		VideoLable:     "疯狂的麦克斯-狂暴之路",
		VideoCanvasUrl: "http://yunshang.oss-cn-beijing.aliyuncs.com/demo/cover/madmax-furyroad.jpg",
	}
	mis = append(mis, mi)
	mi = MovieInfo{
		VideoId:        "d2bbcf7a606605931dfdb5f4e2faf715",
		VideoType:      "4k",
		VideoSource:    "http://yunshang.oss-cn-beijing.aliyuncs.com/demo/4k/mts-out/NinjaTurtles2.mp4",
		VideoLable:     "忍者神龟2-破影而出",
		VideoCanvasUrl: "https://yunshang.oss-cn-beijing.aliyuncs.com/demo/cover/NinjaTurtles_Out-of-the-Shadows.jpg",
	}
	mis = append(mis, mi)

	res := MovieInfoResp{mis, common.RESPONSE_OK}

	/*bys, e := json.Marshal(res)
	if e != nil {
		return
	}*/

	c.JSON(http.StatusOK, res)
}
