package mtsreq

import (
	"encoding/json"
	"ys4k_gin/mtsresp"
)

/*--------------------Call Ali 'Media Process Service' API START-------------------------*/

type SubmitJobsReq struct {
	CommonParam
	Action         string `json:"Action,omitempty"`         // SubmitJobs
	Input          string `json:"Input,omitempty"`          //作业输入 InputFile
	OutputBucket   string `json:"OutputBucket,omitempty"`   //	输出Bucket，需在控制台中完成云资源授权。
	OutputLocation string `json:"OutputLocation,omitempty"` //否		输出 Bucket 所在数据中心，默认值是oss-cn-hangzhou。
	Outputs        string `json:"Outputs,omitempty"`        //Outputs由Output列表构成，JSON数组，大小上限为30  []OutputSimple
	PipelineId     string `json:"PipelineId,omitempty"`     //管道ID，管道的定义详见术语表；若需要异步通知，须保证此管道绑定了可用的消息主题。
	//Input          InputFile      `json:"Input,omitempty"`          //作业输入
	//Outputs        []OutputSimple `json:"Outputs,omitempty"`        //Outputs由Output列表构成，JSON数组，大小上限为30
}

/*
查询转码作业请求
通过转码作业ID，批量查询转码作业，返回默认按CreationTime降序排列。
*/
type QueryJobListReq struct {
	CommonParam
	Action string `json:"Action,omitempty"` //操作接口名，系统规定参数，取值： QueryJobList
	JobIds string `json:"JobIds,omtiempty"` //转码作业ID列表，逗号分隔，一次最多10个
}

/*
取消转码作业请求
Action	String	是	操作接口名，系统规定参数，取值： CancelJob
JobId	String	是	作业ID
*/
type CancelJobReq struct {
	CommonParam
	Action string `json:"Action,omitempty"` // CancelJob
	JobId  string `json:"JobIds,omtiempty"`
}

/*--------------------Call Ali 'Media Process Service' API END-------------------------*/

/******************************************/
/***********实现媒体处理api接口**************/
/******************************************/
func (sj SubmitJobsReq) SendGet() ([]byte, error) {
	return SendGet(sj)
}

func (qjl QueryJobListReq) SendGet() ([]byte, error) {
	return SendGet(qjl)
}
func (cj CancelJobReq) SendGet() ([]byte, error) {
	return SendGet(cj)
}

func (sj SubmitJobsReq) SendGetHandleResp() (resp mtsresp.SubmitJobsResp, e error) {
	respBytes, e := sj.SendGet()
	if e != nil {
		return
	}
	if e = json.Unmarshal(respBytes, &resp); e != nil {
		ylog.Error("Unmarshal_error", e)
		return
	}
	return
}

func (qjl QueryJobListReq) SendGetHandleResp() (resp mtsresp.QueryJobListResp, e error) {
	respBytes, e := qjl.SendGet()
	if e != nil {
		return
	}
	if e = json.Unmarshal(respBytes, &resp); e != nil {
		ylog.Error("Unmarshal_error", e)
		return
	}
	return
}

func (cj CancelJobReq) SendGetHandleResp() (resp mtsresp.CancelJobResp, e error) {
	respBytes, e := cj.SendGet()
	if e != nil {
		return
	}
	if e = json.Unmarshal(respBytes, &resp); e != nil {
		ylog.Error("Unmarshal_error", e)
		return
	}
	return
}

/*******************************************************************/
/***********媒体处理api请求相关struct（非直接请求struct）**************/
/*******************************************************************/
/*
转码作业输入

Bucket		String	是	输入文件所在OSS Bucket，需在控制台中资源控制频道里的Bucket授权页面授予此Bucket读权限给媒体处理服务，遵守OSS Bucket定义，见术语表Bucket
Location	String	是	输入OSS Bucket所在数据中心（OSS Location），遵守OSS Location定义，见术语表Location
Object		String	是	输入文件 （OSS Object），须进行UrlEncode，使用UTF-8编码，遵守OSS Object定义，见术语表Object
Audio		String	否	源媒体文件的音频配置，JSON对象，当输入文件为ADPCM、PCM格式时此参数为必填项，详见21 InputAudio详情，示例
					{
					"Channels":"2",
					"Samplerate":"44100"
					}
Container	String	否	源媒体文件容器配置，JSON对象，当输入文件为ADPCM、PCM格式时此参数为必填项，详见20 InputContainer详情，示例
					{
					"Format":"u8"
					}
*/
type Input struct {
	Bucket    string         `json:"Bucket,omitempty"`
	Location  string         `json:"Location,omitempty"`
	Object    string         `json:"Object,omitempty"`
	Audio     InputAudio     `json:"Audio,omitempty"`
	Container InputContainer `json:"Container,omitempty"`
}

/*
源媒体文件的音频配置
Channels	String	是	源媒体音频声道数,范围[1，8]
Samplerate	String	是	源媒体音频采样率，取值为(0，320000]，单位Hz
*/
type InputAudio struct {
	Channels   string `json:"Channels,omitempty"`
	Samplerate string `json:"Samplerate,omitempty"`
}

/*
源媒体文件容器配置
Format	String	是	源媒体音频格式，
					取值为alaw, f32be, f32le, f64be, f64le, mulaw, s16be, s16le, s24be, s24le, s32be, s32le, s8, u16be, u16le, u24be, u24le, u32be, u32le, u8
*/
type InputContainer struct {
	Format string `json:"Format,omitempty"`
}

/*
作业输出
OutputObject	String		是	输出的文件名（OSS Object），须进行Url Encode，使用UTF-8编码。
								占位符替换示例:转码输入文件若为a/b/c.flv,若OutputObject设置为%7BObjectPrefix%7D%7BFileName%7Dtest.mp4，那么转码输出文件名：a/b/ctest.mp4
								输出文件名支持占位符替换规则
								工作流支持的占位符：代表输入文件前缀的{ObjectPrefix}、代表输入文件名的{FileName}、代表输入文件扩展名的{ExtName}、代表转码输出文件Md5值 的{DestMd5}、代表转码输出文件平均码率的{DestAvgBitrate}，以及代表媒体工作流执行实例ID的{RunId},代表工作流所处理媒体ID的{MediaId}的动态替换。
								非工作流支持的占位符：{ObjectPrefix}、{FileName}、{ExtName}、{DestMd5}、{DestAvgBitrate}
								关于文件扩展名规则
								工作流：根据转码模板容器格式自动在OutputObject后边添加扩展名
								非工作流：不会自动添加扩展名，但如果容器类型为m3u8，则媒体处理服务会给Playlist自动添加扩展名 .m3u8 ，分片文件名会在Playlist后自动加一个从00001开始的5位序列号为后缀并以 - 号相连，文件扩展名为 .ts。
								例如：Playlist文件名为filename.m3u8，则输出第一个ts分片文件为filename-00001.ts。
TemplateId		String		是	转码模板ID，支持自定义转码模板与系统预置模板。
WaterMarks		WaterMark[]	否	JSON数组，水印列表，请参照5 转码水印参数详情，水印数组大小上限为20，既同一路输出最多支持20个水印，示例：
								[{
									"InputFile":{
										"Bucket":"example-bucket",
										"Location":"oss-cn-hangzhou",
										"Object":"example-logo.png"
										},
									"WaterMarkTemplateId":"88c6ca184c0e47098a5b665e2a126797"
								}]
Clip			String		否	JSON对象，剪辑片段，请参照3 Clip详情，示例：
								{
									"TimeSpan":{
									"Seek":"123.45",
									"Duration":"3.45"
									}
								}
Rotate			String		否	视频旋转角度，范围[0，360)，顺时针。
Container		String		否	如设置则覆盖指定转码模版中的对应参数，参考7 Container详情。
Video			String		否	如设置则覆盖指定转码模版中的对应参数,参考8 Video详情。
Audio			String		否	如设置则覆盖指定转码模版中的对应参数，参考10 Audio详情。
AudioStreamMap	String		否	音频流序号,格式：0:a:{序号}，序号从0开始，序号的含义是音频流列表的下标，示例：0:a:0，若不设置，选择默认的音频流
TransConfig		String		否	转码流程配置，如设置则覆盖指定转码模版中的对应参数，参考16 TransConfig详情。
MergeList		String		否	拼接设置，最多支持4个MergeURL，请参考27 拼接参数详情，
								单个拼接片段示例：[{"MergeURL":"http://jvm.oss-cn-hangzhou.aliyuncs.com/tail_comm_01.mp4"}]
								2个拼接片段示例：[{"MergeURL":"http://jvm.oss-cn-hangzhou.aliyuncs.com/tail_comm_01.mp4","Start":"1","Duration":"20"},{"MergeURL":"http://jvm.oss-cn-hangzhou.aliyuncs.com/tail_comm_02.mp4","Start":"5.4","Duration":"10.2"}]
MergeConfigUrl	String		否	MergeList与MergeConfigUrl两个参数只支持二选一，MergeConfigUrl指定的配置文件允许20个拼接片段上限，MergeConfigUrl是拼接配置文件URL地址，示例：http://jvm.oss-cn-hangzhou.aliyuncs.com/mergeConfigfile 只支持存放在OSS上的配置文件，且需要保证有授权给MPS可访问权限，文件内部内容请参考27 拼接参数详情，mergeConfigfile文件内部内容示例：
								{
								"MergeList":[{"MergeURL":"http://jvm.oss-cn-hangzhou.aliyuncs.com/tail_comm.mp4"}]
								}
MuxConfig		String		否	如设置则覆盖指定转码模版中的对应参数，参考17 MuxConfig详情。
Priority		String		否	任务在其对应管道内的转码优先级，范围[1-10]，默认6，最高优先级为10。
UserData		String		否	用户自定义数据，最大长度1024个字节。
M3U8NonStandardSupport	String	否	M3u8非标准支持,JSON对象,参考29 M3u8非标参数详情,示例:
									{
									"TS":{"Md5Support":true,"SizeSupport":true}
									}
Encryption		String		否	数据加密,只支持m3u8格式的输出
								{
								"Type":"hls-aes-128",
								"Key":"ZW5jcnlwdGlvbmtleTEyMw",
								"KeyType":"Base64",
								"KeyUri":"aHR0cDovL2FsaXl1bi5jb20vZG9jdW1lbnQvaGxzMTI4LmtleQ=="
								}
								参考32 Encryption参数详情
SubtitleConfig	String		否	JSON对象，字幕配置，请参照33 SubtitleConfig详情，示例：
								{
								“ExtSubtitleList”:[{
								“Input”:{
								“Bucket”:”example-bucket”,
								“Location”:”oss-cn-hangzhou”,
								“Object”:”example.srt”
								},
								“CharEnc”:”UTF-8”
								}]
								}
OpeningList		String		否	开板列表，JSON列表，见开板详情35。示例:[{"OpenUrl":"http://test-bucket.oss-cn-hangzhou.aliyuncs.com/opening.flv","Start":"1","Width":"1920","Height":"1080"}]
TailSlateList	String		否	尾板列表，JSON列表，见尾板详情36。示例:[{"TailUrl":"http://test-bucket.oss-cn-hangzhou.aliyuncs.com/tail.flv","Start":"1","BlendDuration":"2","Width":"1920","Height":"1080","IsMergeAudio":false,"BgColor":"White"}]
DeWatermark		String		否	模糊处理, JSON对象，见模糊处理详情46。
*/
type Output struct {
	OutputObject           string         `json:"OutputObject,omitempty"`
	TemplateId             string         `json:"TemplateId,omitempty"`
	WaterMarks             []WaterMark    `json:"WaterMarks,omitempty"`
	Clip                   Clip           `json:"Clip,omitempty"`
	Rotate                 string         `json:"Rotate,omitempty"`
	Container              Container      `json:"Container,omitempty"`
	Video                  Video          `json:"Video,omitempty"`
	Audio                  Audio          `json:"Audio,omitempty"`
	AudioStreamMap         string         `json:"AudioStreamMap,omitempty"`
	TransConfig            TransConfig    `json:"TransConfig,omitempty"`
	MergeList              []MergeUrl     `json:"MergeList,omitempty"`
	MuxConfig              MuxConfig      `json:"MuxConfig,omitempty"`
	Priority               string         `json:"Priority,omitempty"`
	UserData               string         `json:"UserData,omitempty"`
	M3U8NonStandardSupport M3u8           `json:"M3U8NonStandardSupport,omitempty"`
	SubtitleConfig         SubtitleConfig `json:"SubtitleConfig,omitempty"`
	OpeningList            []OpenUrl      `json:"OpeningList,omitempty"`
	TailSlateList          []TailUrl      `json:"TailSlateList,omitempty"`
	//DeWatermark            DeWatermark
	//Encryption             Encryption     `json:"Encryption,omitempty"`
}
type OutputSimple struct {
	OutputObject string `json:"OutputObject,omitempty"`
	TemplateId   string `json:"TemplateId,omitempty"`
}

/*
尾板配置
TailUrl			String	是	尾板视频的OSS URL地址
BlendDuration	String	否	正片视频和尾板视频过渡的时长。过渡的效果是淡入淡出：正片显示最后一帧，同时播放尾板视频，正片最后一帧逐步变暗，尾板视频逐步变亮。单位秒，默认0。
Width			String	否	宽，范围（0，4096）、-1、full,-1代表片源的值，full表示填满画面。默认为-1
Height			String	否	高，范围（0，4096）、-1、full,-1代表片源的值，full表示填满画面。默认为-1
IsMergeAudio	Boolean	否	是否要拼接尾板视频的音频内容，默认为true
BgColor			String	否	如果尾板视频的宽、高小于正片时，设置空白处填充的背景色。默认为White，取值见bgcolor
*/
type TailUrl struct {
	TailUrl       string `json:"TailUrl,omitempty"`
	BlendDuration string `json:"BlendDuration,omitempty"`
	Width         string `json:"Width,omitempty"`
	Height        string `json:"Height,omitempty"`
	IsMergeAudio  bool   `json:"IsMergeAudio,omitempty"`
	BgColor       string `json:"BgColor,omitempty"`
}

/*
开板配置
OpenUrl	String	是	开板视频的OSS URL地址
Start	String	否	相对正片视频的开始时间。从0开始延迟多长时间后，显示开板视频。单位秒，默认0。
Width	String	否	宽，范围（0，4096）、-1、full,-1代表片源的值，full表示填满画面。默认为-1
Height	String	否	高，范围（0，4096）、-1、full,-1代表片源的值，full表示填满画面。默认为-1
*/
type OpenUrl struct {
	OpenUrl string `json:"OpenUrl,omitempty"`
	Start   string `json:"Start,omitempty"`
	Width   string `json:"Width,omitempty"`
	Height  string `json:"Height,omitempty"`
}

/*
字幕配置
ExtSubtitleList	ExtSubtitle[]	否	JSON数组，最多4个，外部字幕列表。详见34 ExtSubtitle，示例：
[
{
“Input”:{
“Bucket”:”example-bucket”,
“Location”:”oss-cn-hangzhou”,
“Object”:”example.srt”
},
“CharEnc”:”UTF-8”
}
]
*/
type SubtitleConfig struct {
	ExtSubtitleList []ExtSubtitle `json:"ExtSubtitleList,omitempty"`
}

/*
Input	String	是	JSON对象，外部输入字幕文件，目前支持srt、ass格式，参考1 Input详情，示例：
					{
					“Bucket”:”example-bucket”,
					“Location”:”oss-cn-hangzhou”,
					“Object”:”example.srt”
					}
					目前支持{ObjectPrefix}、{FileName}、{ExtName}动态替换，示例：转码输入文件Object：a/b/c/test.flv，字幕文件用动态规则可表示为：{ObjectPrefix}{FileName}-cn.srt,需经URLEncode，Object设置为：%7bObjectPrefix%7d%7bFileName%7d-cn.srt。那么MPS会认为外挂字幕文件地址为：a/b/c/test-cn.srt
CharEnc	String	否	外部字幕字符编码，范围UTF-8, GBK, BIG5, auto,默认auto，外部字幕字符编码设置为auto时，存在误检情况，建议用户指定具体的字符编码。
*/
type ExtSubtitle struct {
	Input   InputFile `json:"Input,omitempty"`
	CharEnc string    `json:"CharEnc"`
}

/*
加密配置
Type	String	是	取值: hls-aes-128
Key		String	是	加密视频的密钥，需加密，方式见KeyType, 如密钥为”encryptionkey128”, 则Base64(“encryptionkey128”)， 或 KMS(Base64(“encryptionkey128”))
KeyUri	String	是	密钥的访问url，使用BASE64进行编码
KeyType	String	是	密钥Key不能明文传输给MPS，需要加密，方式为 Base64 或 KMS 如使用KMS，请联系我们，我们提供主密钥, Base64为基础加密方式，KMS方式是在基础方式上，又使用KMS进行加密
*/
type Encryption struct {
	Type    string `json:"Type,omitempty"`
	Key     string `json:"Key,omitempty"`
	KeyUri  string `json:"KeyUri,omitempty"`
	KeyType string `json:"KeyType,omitempty"`
}

/*
M3u8非标准支持配置
*/
type M3u8 struct {
	TS TS `json:"TS,omitempty"` //TS	String	否	JSON对象，TS文件相关非标准支持，参考 30 TS参数支持详情
}

/*
Md5Support	Boolean	否	是否支持在m3u8文件中输出ts的md5值
SizeSupport	Boolean	否	是否支持在m3u8文件中输出ts文件的大小
*/
type TS struct {
	Md5Support  bool `json:"Md5Support,omitempty"`
	SizeSupport bool `json:"SizeSupport,omitempty"`
}

/*
拼接设置
MergeURL		String	是	拼接片段地址，示例：http://example-bucket.oss-cn-hangzhou.aliyuncs.com/example-object.flv，Object需要经过url encode，采用utf-8编码
Start			String	否	起始时间点，格式：hh:mm:ss[.SSS]或者sssss[.SSS]，示例：01:59:59.999或者32000.23
Duration		String	否	持续时间，格式：hh:mm:ss[.SSS]或者sssss[.SSS]，示例：01:59:59.999或者32000.23
*/
type MergeUrl struct {
	MergeURL string `json:"MergeURL,omitempty"`
	Start    string `json:"Start,omitempty"`
	Duration string `json:"Duration,omitempty"`
}

/*
剪辑配置
TimeSpan				String	否	剪辑时间区间，请参照4 TimeSpan详情
ConfigToClipFirstPart	Boolean	否	是否剪辑第一片，
									false：拼接完后剪辑，默认
									true：先剪辑第一片后拼接
*/
type Clip struct {
	TimeSpan              string `json:"TimeSpan,omitempty"`
	ConfigToClipFirstPart bool   `json:"ConfigToClipFirstPart,omitempty"`
}

/*
水印配置
WaterMarkTemplateId	String	否	水印模板ID，若不设置，则使用水印模板的默认配置：水印位置TopRight，偏移量Dx、Dy取0，水印宽为输出分辨率宽的0.12倍，水印高为相对水印宽的等比缩放。
InputFile			String	是	水印输入文件，见InputFile详情，目前支持png图片、mov文件作为输入。
Width				String	否	若设置，则此值覆盖水印模板对应水印图片宽，值有两种形式：整数型代水印图片宽的像素值，范围[8，4096]，单位px；小数型代表相对输出视频分辨率宽的比率，范围(0,1)，支持4位小数，如0.9999，超出部分系统自动丢弃。
Height				String	否	若设置，则此值覆盖水印模板对应水印图片高，值有两种形式：整数型代表水印图片高的像素值，范围[8，4096]，单位px；小数型代表相对输出视频分辨率高的比率，范围(0，1)，支持4位小数，如0.9999，超出部分系统自动丢弃。
Dx					String	否	若设置，则此值覆盖水印模板对应参数，水印图片相对输出视频的水平偏移量，默认值是0；值有两种形式：整数型代表偏移像素，范围[8，4096]，单位px；小数型代表水平偏移量与输出分辨率宽的比率，范围(0，1)，支持4位小数，如0.9999，超出部分系统自动丢弃。
Dy					String	否	若设置，则此值覆盖水印模板对应参数，水印图片相对输出视频的垂直偏移量，默认值是0；值有两种形式：整数型代表偏移像素，范围[8，4096]，单位px；小数型代表垂直偏移量与输出分辨率高的比率，范围(0，1)，支持4位小数，如0.9999，超出部分系统自动丢弃。
ReferPos			String	否	若设置，则此值覆盖水印模板对应参数，水印的位置，值范围TopRight、TopLeft、BottomRight、BottomLeft。
Type				String	否	若设置，则此值覆盖水印模板对应参数，水印类型，Image、Text，默认为Image，Image:图片水印，Text:文字水印，若填写Text，则必须填写TextWaterMark字段
Timeline			String	否	若设置，则此值覆盖水印模板对应参数，动态水印，见Timeline详情
TextWaterMark		String	否	JSON对象，文字水印配置，若类型为文字水印，则此配置项不能为空，详见37 文字水印参数详情，示例：{“Content”:”5rWL6K+V5paH5a2X5rC05Y2w”,”Top”:2,”Left”:10}
*/
type WaterMark struct {
	WaterMarkTemplateId string        `json:"WaterMarkTemplateId,omitempty"`
	InputFile           InputFile     `json:"InputFile,omitempty"`
	Width               string        `json:"Width,omitempty"`
	Height              string        `json:"Height,omitempty"`
	Dx                  string        `json:"Dx,omitempty"`
	Dy                  string        `json:"Dy,omitempty"`
	ReferPos            string        `json:"ReferPos,omitempty"`
	Type                string        `json:"Type,omitempty"`
	Timeline            string        `json:"Timeline,omitempty"`
	TextWaterMark       TextWaterMark `json:"TextWaterMark,omitempty"`
}
type InputFile struct {
	Bucket   string `json:"Bucket,omitempty"`
	Location string `json:"Location,omitempty"`
	Object   string `json:"Object,omitempty"`
}

/*
文字水印配置
Content		String	是	文字水印内容，内容需做Base64编码，示例：若想添加文字水印”测试文字水印”，那么Content的值为：5rWL6K+V5paH5a2X5rC05Y2w
FontName	String	否	默认”SimSun”,支持的字体
FontSize	Int		否	字体大小，默认16，范围：(4, 120)
FontColor	String	否	字体颜色，取值见FontColor
FontAlpha	Int		否	字体透明度，范围：(0, 1]，默认1.0
Top			Int		否	文本上边距，默认0，范围：[0,]
Left		Int		否	文本左边距，默认0，范围：[0,]
BorderWidth	Int		否	描边宽度，默认0，范围：[0,]
BorderColor	String	否	描边颜色，默认：Black，取值 BorderColor
*/
type TextWaterMark struct {
	Content     string `json:"Content,omitempty"`
	FontName    string `json:"FontName,omitempty"`
	FontSize    int    `json:"FontSize,omitempty"`
	FontColor   string `json:"FontColor,omitempty"`
	FontAlpha   int    `json:"FontAlpha,omitempty"`
	Top         int    `json:"Top,omitempty"`
	Left        int    `json:"Left,omitempty"`
	BorderWidth int    `json:"BorderWidth,omitempty"`
	BorderColor string `json:"BorderColor,omitempty"`
}
