package mtsreq

import (
	"encoding/json"
	"ys4k_gin/mtsresp"
)

/*************************************************************************/
/*--------------------调用阿里媒体处理API请求 START------------------------*/
/*************************************************************************/
//新增自定义转码模板请求
type AddTemplateReq struct {
	CommonParam
	Action      string      `json:"Action,omitempty"`      //操作接口名，系统规定参数，取值：AddTemplate
	Name        string      `json:"Name,omitempty"`        //模板名称，最大长度128字节
	Container   Container   `json:"Container,omitempty"`   //否 容器，JSON对象，见附录 参数Container详情。例如：{"Format":"mp4"}
	Video       Video       `json:"Video,omitempty"`       //否 不设置Video参数则转码输出不包括视频流；如需保留视频流则必须设置此对象。
	Audio       Audio       `json:"Audio,omitempty"`       //否 不设置Audio参数则转码输出不包括音频流；如需保留音频流则必须设置此对象
	TransConfig TransConfig `json:"TransConfig,omitempty"` //否 转码通用配置
	MuxConfig   MuxConfig   `json:"MuxConfig,omitempty"`   //否 封包配置
}

/*
搜索指定状态的自定义模板
Action		String	是	操作接口名，系统规定参数，取值： SearchTemplate
PageNumber	Long	否	当前页号，从第1页开始，默认值是1。
PageSize	Long	否	分页查询时设置的每页大小，默认值是10，上限100。
State		String	否	转码模板状态：All表示所有，Normal表示正常，Deleted表示已删除，默认是All。
*/
type SearchTemplateReq struct {
	CommonParam
	Action     string
	PageNumber int64  `json:"PageNumber,omitempty"`
	PageSize   int64  `json:"PageSize,omitempty"`
	State      string `json:"State,omitempty"`
}

/*
查询指定模板
Action		String	是	操作接口名，系统规定参数，取值： QueryTemplateList
TemplateIds	String	是	模板ID列表，最多一次查10个，逗号分隔。
*/
type QueryTemplateListReq struct {
	CommonParam
	Action      string
	TemplateIds string
}

/******************************************/
/***********实现媒体处理api接口**************/
/******************************************/
func (at AddTemplateReq) SendGet() ([]byte, error) {
	return SendGet(at)
}
func (st SearchTemplateReq) SendGet() ([]byte, error) {
	return SendGet(st)
}

func (qt QueryTemplateListReq) SendGet() ([]byte, error) {
	return SendGet(qt)
}

func (req AddTemplateReq) SendGetHandleResp() (resp mtsresp.AddTemplateResp, e error) {
	respBytes, e := req.SendGet()
	if e != nil {
		return
	}
	if e = json.Unmarshal(respBytes, &resp); e != nil {
		ylog.Error("Unmarshal_error", e)
		return
	}
	return
}

/*******************************************************************/
/***********媒体处理api请求相关struct（非直接请求struct）**************/
/*******************************************************************/
/*
视频流配置
Codec			String	否	编解码格式	支持H.264、H.265、GIF、WEBP，默认值H.264
Profile			String	否	编码级别	支持baseline、main、high，默认值high,
							baseline:适合移动设备，
							main:适合标准分辨率设备，
							high:适合高分辨率设备。
							最佳实践:如果您有多个清晰度，建议最低清晰度配成baseline，以保证最低端设备可播放，其他清晰度配置为main或high
							目前仅H.264支持此参数
Bitrate			String	否	视频输出文件的码率	值范围[10，50000]，单位Kbps。
Crf				String	否	码率-质量控制因子	值范围[0，51]，默认值26。如果设置了Crf，则Bitrate的设置失效。
Width			String	否	宽	默认值是视频原始宽度，值范围[128，4096]，单位px。
Height			String	否	高	默认值是视频原始高度，值范围[128，4096]，单位px。
Fps				String	否	帧率	默认值取输入文件帧率，当输入文件帧率超过60时取60, 值范围(0,60]，单位fps。
Gop				String	否	关键帧间最大时间间隔或者最大帧数	最大时间间隔时，必传单位s，默认值10s
							最大帧数时，无单位，值范围[1,100000]
Preset			String	否	视频算法器预置	支持veryfast、fast、medium、slow、slower，默认值medium。
							目前仅H.264支持此参数
ScanMode		String	否	扫描模式	支持interlaced、progressive。
Bufsize			String	否	缓冲区大小	值范围[1000，128000]，默认值6000，单位Kb。
Maxrate			String	否	视频码率峰值	值范围[10，50000]，单位Kbps。
PixFmt			String	否	视频颜色格式	范围yuv420p，yuvj420p等标准颜色格式，默认值yuv420p或原始颜色格式。
Remove			String	否	是否删除视频流	true表示删除，false表示保留，默认值false
Crop			String	否	视频画面裁切	支持2种方式。 1）自动检测黑边并裁切，设置为”border” 2）自定义裁切，参数格式：width:height:left:top。 示例：1280:800:0:140
Pad				String	否	视频贴黑边	参数格式：width:height:left:top。 示例：1280:800:0:140
LongShortMode	String	否	是否开启横竖屏自适应（即：长短边模式）	转码输出的宽对应输入片源的长边（竖屏为片源的高），高对应输入视频的短边（竖屏为片源的宽），true表示开启，false表示关闭，默认值false

*/
type Video struct {
	Codec         string `json:"Codec,omitempty"`
	Profile       string `json:"Profile,omitempty"`
	Bitrate       string `json:"Bitrate,omitempty"`
	Crf           string `json:"Crf,omitempty"`
	Width         string `json:"Width,omitempty"`
	Height        string `json:"Height,omitempty"`
	Fps           string `json:"Fps,omitempty"`
	Gop           string `json:"Gop,omitempty"`
	Preset        string `json:"Preset,omitempty"`
	ScanMode      string `json:"ScanMode,omitempty"`
	Bufsize       string `json:"Bufsize,omitempty"`
	Maxrate       string `json:"Maxrate,omitempty"`
	PixFmt        string `json:"PixFmt,omitempty"`
	Remove        string `json:"Remove,omitempty"`
	Crop          string `json:"Crop,omitempty"`
	Pad           string `json:"Pad,omitempty"`
	LongShortMode string `json:"LongShortMode,omitempty"`
}

/*
音频流配置
Codec		String	否	音频编解码格式，AAC、MP3、VORBIS、FLAC，默认是AAC。
Profile		String	否	音频编码预置，当Codec为 AAC时，范围aac_low、aac_he、aac_he_v2、aac_ld、aac_eld。
Samplerate	String	否	采样率，默认值是44100，支持22050、32000、44100、48000、96000，单位为Hz。
						若视频容器格式为flv，音频编解码格式选择为mp3时，采样率不支持32000、48000、96000；音频编解码格式为mp3时，采样率不支持96000
Bitrate		String	否	输出文件的音频码率，值范围[8，1000]，默认值128，单位Kbps。
Channels	String	否	声道数，默认值是2；当Codec设置为 mp3 时，声道数只支持1、2;当Codec设置为 aac 时，声道数只支持1、2、4、5、6、8
Remove		String	否	是否删除音频流，true表示删除，false表示保留，默认值false
*/
type Audio struct {
	Codec      string `json:"Codec,omitempty"`
	Profile    string `json:"Profile,omitempty"`
	Samplerate string `json:"Samplerate,omitempty"`
	Bitrate    string `json:"Bitrate,omitempty"`
	Channels   string `json:"Channels,omitempty"`
	Remove     string `json:"Remove,omitempty"`
}

/*
容器格式，默认值为mp4。
视频转码支持flv、mp4、HLS（m3u8+ts）、MPEG-DASH（MPD+fMP4）
音频转码支持mp3、mp4、ogg、flac、m4a
图片支持gif、webp
容器格式为gif时，Video Codec设置只能设置为GIF，
容器格式为webp时，Video Codec设置只能设置为WEBP，
容器格式为flv时，Video Codec不能设置为H.265。
*/
type Container struct {
	Format string `json:"Format,omitempty"`
}

/*
转码通用配置
TransMode				String	否	转码模式，默认值onepass，可选范围onepass、twopass、CBR。
AdjDarMethod			String	否	分辨率改写方式，默认值none,可选范围rescale、crop、pad、none
IsCheckReso				String	否	是否检查分辨率，如果输出分辨率大于输入分辨率（判断条件是宽或高），则输出分辨率等于输入分辨率。true表示检查，false表示不检查，默认值false。
IsCheckResoFail			String	否	是否检查分辨率，如果输出分辨率大于输入分辨率（判断条件是宽或高），则返回转码失败。true表示检查，false表示不检查，默认值false
IsCheckVideoBitrate		String	否	是否检查视频码率，如果视频输出码率大于视频输入码率，则视频输出码率等于视频输入码率。true表示检查，false表示不检查，默认值false
IsCheckAudioBitrate		String	否	是否检查音频码率，如果音频输出码率大于音频输入码率，则音频输出码率等于音频输入码率。true表示检查，false表示不检查，默认值false
isCheckAudioBitrateFail	String	否	当输出音频码率大于媒体源音频码率时，true表示，不进行转码，false表示不检查，默认值false，此值优先级大于IsCheckAudioBitrate.
isCheckVideoBitrateFail	String	否	当输出视频码率大于媒体源视频码率时，true表示，不进行转码，false表示不检查，默认值false，此值优先级大于IsCheckVideoBitrate.
*/
type TransConfig struct {
	TransMode               string `json:"TransMode,omitempty"`
	AdjDarMethod            string `json:"AdjDarMethod,omitempty"`
	IsCheckReso             string `json:"IsCheckReso,omitempty"`
	IsCheckResoFail         string `json:"IsCheckResoFail,omitempty"`
	IsCheckVideoBitrate     string `json:"IsCheckVideoBitrate,omitempty"`
	IsCheckAudioBitrate     string `json:"IsCheckAudioBitrate,omitempty"`
	isCheckAudioBitrateFail string `json:"isCheckAudioBitrateFail,omitempty"`
	isCheckVideoBitrateFail string `json:"isCheckVideoBitrateFail,omitempty"`
}

/*
封包配置

*/
type MuxConfig struct {
	Segment Segment `json:"Segment,omitempty"`
}

/*
Duration		String	否	分片时长，整数值，单位：秒，范围[1，60]。
ForceSegTime	String	否	指定分片时间列表，逗号分隔，最多可指定10个分片时间点，整数值，单位：秒。示例:23、55、60 代表在23，55，60秒处分片
*/
type Segment struct {
	Duration     string `json:"Duration,omitempty"`
	ForceSegTime string `json:"ForceSegTime,omitempty"`
}
