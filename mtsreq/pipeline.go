package mtsreq

import (
	"encoding/json"
	"ys4k_gin/mtsresp"
)

/*************************************************************************/
/*--------------------调用阿里媒体处理API请求 START------------------------*/
/*************************************************************************/

//更新管道请求
type UpdatePipelineReq struct {
	CommonParam
	Action       string `json:"Action,omitempty"`       //是 操作接口名，系统规定参数，取值：UpdatePipeline
	PipelineId   string `json:"PipelineId,omitempty"`   //是 管道ID
	Name         string `json:"Name,omitempty"`         //是 管道名称，最大长度128字节。
	State        string `json:"State,omitempty"`        //是 管道状态，分为Active、Paused；
	NotifyConfig string `json:"NotifyConfig,omitempty"` //否 MNS配置，例：{"Topic":"mts-topic-1"}。
	Role         string `json:"Role,omitempty"`         //否 角色
}

/******************************************/
/***********实现媒体处理api接口**************/
/******************************************/
func (up UpdatePipelineReq) SendGet() ([]byte, error) {
	return SendGet(up)
}

func (up UpdatePipelineReq) SendGetHandleResp() (resp mtsresp.UpdatePipelineResp, e error) {
	respBytes, e := up.SendGet()
	if e != nil {
		return
	}
	if e = json.Unmarshal(respBytes, &resp); e != nil {
		ylog.Error("Unmarshal_error", e)
		return
	}
	return
}
