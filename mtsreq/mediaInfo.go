package mtsreq

import (
	"encoding/json"
	"ys4k_gin/mtsresp"
)

/*************************************************************************/
/*--------------------调用阿里媒体处理API请求 START------------------------*/
/*************************************************************************/
//新增自定义转码模板请求
type MediaInfoReq struct {
	CommonParam
	Action string `json:"Action,omitempty"` //操作接口名，系统规定参数，取值：SubmitMediaInfoJob
	Input  string `json:"Input,omitempty"`
}

/******************************************/
/***********实现媒体处理api接口**************/
/******************************************/
func (req MediaInfoReq) SendGet() ([]byte, error) {
	return SendGet(req)
}

func (req MediaInfoReq) SendMediaInfoReq() (resp mtsresp.MediaInfoResp, e error) {
	respBytes, e := req.SendGet()
	if e != nil {
		return
	}

	if e = json.Unmarshal(respBytes, &resp); e != nil {
		ylog.Error("Unmarshal_error", e)
		return
	}
	return
}

/*******************************************************************/
/***********媒体处理api请求相关struct（非直接请求struct）**************/
/*******************************************************************/
