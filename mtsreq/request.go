/*
mts request:common struct, params and functions.
*/
package mtsreq

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"

	"ys4k_gin/common"
	"ys4k_gin/logger"
	"ys4k_gin/mtssign"
	"ys4k_gin/ysdb"
)

var (
	ylog *logger.Logger
	db   *gorm.DB
)

func Init() {
	logger, err := logger.NewLogger(&logger.LoggerConf{
		Dir:          "../logs",
		Filename:     common.YS_LOG_MTS_REQUEST,
		DailyRolling: true,
		SizeRolling:  true,
		MaxFileSize:  50 * logger.MB,
		MaxFileCount: 10,
		Withprefix:   true,
	})

	if err != nil {
		fmt.Printf("NewLogger error: ", err)
		os.Exit(1)
	}
	ylog = logger
	db = ysdb.DB

}

//accessKey
const (
	ACCESS_KEY_ID     = "LTAIcyr52G7olaLH"
	ACCESS_KEY_SECRET = "oGsrCRAAe6a95wqj5dnCoWwaxUy3gJ"
)

//define Ali MTS Request interface
type MtsRequset interface {
	SendGet() (resp []byte, err error)
}

//mts request common params
type CommonParam struct {
	Format           string //返回值的类型，支持JSON与XML，默认为XML。
	Version          string //API版本号，为日期形式：YYYY-MM-DD，本版本对应为2014-06-18
	AccessKeyId      string //阿里云颁发给用户的访问服务所用的密钥ID
	Signature        string //签名结果串，关于签名的计算方法，参见 签名机制
	SignatureMethod  string //签名方式，目前支持HMAC-SHA1
	Timestamp        string //请求的时间戳。日期格式按照ISO8601标准表示，并需要使用UTC时间。格式为：YYYY-MM-DDThh:mm:ssZ eg:2014-7-29T12:00:00Z(为北京时间2014年7月29日的20点0分0秒。
	SignatureVersion string //签名算法版本，目前版本是1.0
	SignatureNonce   string //唯一随机数，用于防止网络重放攻击。用户在不同请求间要使用不同的随机数值
}

/*
get common params with default value
*/
func NewCommonParam() CommonParam {
	ts := time.Now().UTC().Format(time.RFC3339)
	nonce := common.RandStringBytesMaskImpr(32)
	return CommonParam{
		Format:           "JSON",
		Version:          "2014-06-18",
		AccessKeyId:      ACCESS_KEY_ID,
		Signature:        "",
		SignatureMethod:  "HMAC-SHA1",
		Timestamp:        ts,
		SignatureVersion: "1.0",
		SignatureNonce:   nonce,
	}
}

/*
Sort and Encode mts request fields, get the sign string that need to be urlEncoded.
*/
func MtsReqSign(data MtsRequset) (sign string, e error) {
	httpMethod := "GET"
	separator := "&"
	equal := "="

	//turn mts request struct to param map
	bys, e := json.Marshal(data)
	if e != nil {
		fmt.Println("jsonMarshal:", e)
		return
	}
	m := make(map[string]interface{})
	if e = json.Unmarshal(bys, &m); e != nil {
		fmt.Println("jsonUNMarshalMap:", e)
		return
	}

	//fmt.Println("paramMap:", m)
	//save param map's keys into slice
	keys := make([]string, len(m))
	i := 0
	for k := range m {
		keys[i] = k
		i++
	}

	//sort key slice
	sort.Strings(keys)

	//fmt.Println("keys:", keys)

	var buffer bytes.Buffer
	buffer.WriteString(httpMethod)
	buffer.WriteString(separator)
	buffer.WriteString(PercentEncode("/"))
	buffer.WriteString(separator)

	for i, p := range keys {
		//'signature' param should not in the sign string
		if p == "Signature" {
			continue
		}
		//construct the sign string
		if i != 0 { //befor the first param, there is no “&”
			buffer.WriteString(PercentEncode(separator))
		}
		buffer.WriteString(PercentEncode(p))
		buffer.WriteString(PercentEncode(equal))

		//After processed by 'json.Marshal',the int or float fields will turn to float64. Turn them to string here.
		switch t := m[p].(type) {
		case string:
			//encode twice: 1. encoded as init param, 2. encoded as sign param.
			buffer.WriteString(PercentEncode(PercentEncode(t)))
		case float64:
			buffer.WriteString(PercentEncode(strconv.FormatFloat(t, 'E', -1, 64)))
		}
	}
	//toSign = PercentEncode(buffer.String())
	toSign := buffer.String()
	//fmt.Println("toSign:", toSign)
	sign = mtssign.SHA1(toSign, ACCESS_KEY_SECRET)
	return
}

func MtsUrlFormat(req interface{}) (uriStr string, e error) {
	//请求struct转换为参数map
	bys, e := json.Marshal(req)
	if e != nil {
		fmt.Println("jsonMarshal:", e)
		return
	}
	m := make(map[string]interface{})
	if e = json.Unmarshal(bys, &m); e != nil {
		fmt.Println("jsonUNMarshalMap:", e)
		return
	}
	//fmt.Println("url:map::", m)
	var buf bytes.Buffer
	for k, v := range m {
		buf.WriteString(k)
		buf.WriteString("=")

		//After processed by 'json.Marshal',the int or float fields will turn to float64. Turn them to string here.
		switch t := v.(type) {
		case string:
			buf.WriteString(PercentEncode(t))
		case float64:
			buf.WriteString(PercentEncode(strconv.FormatFloat(t, 'E', -1, 64)))
		}
		buf.WriteString("&")
	}
	s := buf.String()
	uriStr = s[0 : len(s)-1]
	return
}

/*
对text进行urlencode,并做特殊处理[" ","]
需要说明的是英文空格（ ）要被编码是%20，而不是加号（+）;
把编码后的字符串中加号（+）替换成%20、星号（*）替换成%2A，%7E替换回波浪号(~)，即可得到上述规则描述的编码字符串。
*/
func PercentEncode(text string) string {
	initStr := url.QueryEscape(text)
	str := initStr
	if strings.Contains(initStr, "+") {
		str = strings.Replace(initStr, "+", "%20", -1)
	}
	if strings.Contains(initStr, "~") {
		str = strings.Replace(initStr, "~", "%7E", -1)
	}
	if strings.Contains(initStr, "*") {
		str = strings.Replace(initStr, "*", "%2A", -1)
	}

	return str
}

func SendGet(req MtsRequset) (respBytes []byte, e error) {
	sign, e := MtsReqSign(req)
	if e != nil {
		fmt.Println("error1:", e)
		return
	}
	switch t := req.(type) {
	case SearchTemplateReq:
		t.Signature = sign
		req = t
	case QueryTemplateListReq:
		t.Signature = sign
		req = t
	case SubmitJobsReq:
		t.Signature = sign
		req = t
	case QueryJobListReq:
		t.Signature = sign
		req = t
	case MediaInfoReq:
		t.Signature = sign
		req = t
	}

	uriStr, e := MtsUrlFormat(req)
	if e != nil {
		fmt.Println("error2:", e)
		return
	}

	resp, e := http.Get("http://mts.cn-beijing.aliyuncs.com/?" + uriStr)
	//fmt.Println("final Url :\n", "http://mts.cn-beijing.aliyuncs.com/?"+uriStr, "\n")
	if e != nil {
		//fmt.Println("responsesss error: ", e)
		ylog.Error("sendGet_error: ", e, "|", req)
		return
	}
	defer resp.Body.Close()
	respBytes, e = ioutil.ReadAll(resp.Body)
	if e != nil {
		ylog.Error("readall error:", e)
		return
	}
	return
}
