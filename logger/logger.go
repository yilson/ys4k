// Package logger is used to write log.
// It support writing file and writing console with prefix(mean:data, time, file-name, line-number) or not.
// If set writing file, the file can roll by data and size.
//
// A example is:
//
// package main
//
// import (
// 	"fmt"
//
// 	"cloutropy.com/modules/logger"
// )
//
// func main() {
//   //new a instance log1. output log to file and console.
//   log1, _ := logger.NewLogger(&logger.LoggerConf{
//      LogLevel:        "ALL",
// 		Dir:             "/home/admin/logs",
// 		Filename:        "runlog.log",
// 		ConsoleAppender: true,
// 		DailyRolling:    true,
// 		SizeRolling:     true,
// 		MaxFileSize:     1 * logger.GB,
// 		MaxFileCount:    7,
// 		Withprefix:      true,
// 	})
//
//  //new a instance log2. just out log to file.
// 	log2, _ := logger.NewLogger(&logger.LoggerConf{
// 		Dir:             "/home/admin/logs",
// 		Filename:        "monitor.log",
// 		DailyRolling:    true,
// 		SizeRolling:     true,
// 		MaxFileSize:     200 * logger.MB,
// 		MaxFileCount:    7,
// 	})
//
//  //new a instance log3. just output log to console.
//  log3, _ := logger.NewLogger(&logger.LoggerConf{
// 		LogLevel:        "ALL",
// 		ConsoleAppender: true,
// 		Withprefix:      true,
//  })
//
//
// 	log1.Debug("1 DebugDebugDebugDebugDebugDebugDebugDebugDebugDebug")
// 	log1.Info("2 InfoInfoInfoInfoInfoInfoInfoInfoInfoInfoInfoInfoInfo")
// 	log1.Warn("3 WarnWarnWarnWarnWarnWarnWarnWarnWarnWarnWarnWarnWarn")
// 	log1.Error("4 ErrorErrorErrorErrorErrorErrorErrorErrorErrorError")
// 	log1.Fatal("5 FatalFatalFatalFatalFatalFatalFatalFatalFatalFatal")
//
// 	log2.Log("LogLogLogLogLogLogLogLogLogLogLogLogLogLogLogLogLogLog")
//
// 	log3.Debug("6 DebugDebugDebugDebugDebugDebugDebugDebugDebugDebug")
// 	log3.Info("7 InfoInfoInfoInfoInfoInfoInfoInfoInfoInfoInfoInfoInfo")
// 	log3.Warn("8 WarnWarnWarnWarnWarnWarnWarnWarnWarnWarnWarnWarnWarn")
// 	log3.Error("9 ErrorErrorErrorErrorErrorErrorErrorErrorErrorError")
// 	log3.Fatal("10 FatalFatalFatalFatalFatalFatalFatalFatalFatalFatal")
// }

package logger

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"
)

type LEVEL int32

const (
	ALL LEVEL = iota
	DEBUG
	INFO
	WARN
	ERROR
	FATAL
	OFF
)

const (
	KB = int64(1024)
	MB = 1024 * KB
	GB = 1024 * MB
	TB = 1024 * GB
)

const (
	TIMEFORMAT = "2006-01-02-15:04:05"
	DATEFORMAT = "2006-01-02"
)

type _FILE struct {
	dir      string
	filename string
	mu       *sync.RWMutex
	logfile  *os.File
	lg       *log.Logger
}

type Logger struct {
	logLevel        LEVEL
	maxFileSize     int64
	maxFileCount    int32
	dailyRolling    bool
	sizeRolling     bool
	consoleAppender bool
	fileAppender    bool
	withPrefix      bool
	logObj          *_FILE
}

type LoggerConf struct {
	// All log level form low to high :ALL,DEBUG,INFO,WARN,ERROR,FATAL,OFF.
	// default is ALL.
	LogLevel string

	// the dir of log file.
	Dir string

	// the name of log file.
	// if both Dir and Filename are not null, file will be write;
	// if both Dir and Filename are null, file will not be write;
	// else will be error.
	Filename string

	// log whether appender to console. default false.
	ConsoleAppender bool

	// log file whether rolling by daily. default false.
	DailyRolling bool

	// log file whether rolling by size. default false.
	SizeRolling bool

	// if SizeRolling is true, the max file size to roll. can use KB,MB,TB. default 0.
	MaxFileSize int64

	// if log file is rolling, max the sum of log files. default 0.
	MaxFileCount int32

	//The prefix appears at the beginning of each generated log line.
	//The flag argument defines the logging properties.
	Withprefix bool
}

func NewLogger(conf *LoggerConf) (*Logger, error) {
	log := &Logger{}

	log.setLogLevel(conf.LogLevel)

	log.consoleAppender = conf.ConsoleAppender
	log.withPrefix = conf.Withprefix
	log.fileAppender = true
	if conf.Dir == "" && conf.Filename == "" {
		log.fileAppender = false
		return log, nil
	}
	if conf.Dir == "" || conf.Filename == "" {
		return nil, errors.New("dir and filename are not null.")
	}

	log.maxFileCount = conf.MaxFileCount
	log.dailyRolling = conf.DailyRolling
	log.sizeRolling = conf.SizeRolling
	log.maxFileSize = conf.MaxFileSize

	if log.sizeRolling && log.maxFileSize <= 0 {
		log.maxFileSize = 1 * GB //default maxFileSize is 1GB.
	}

	if log.maxFileCount <= 0 {
		log.maxFileCount = 7 // default maxFileCount is 7.
	}

	log.logObj = &_FILE{dir: conf.Dir, filename: conf.Filename, mu: new(sync.RWMutex)}

	if err := log.createFile(false); err != nil {
		return nil, err
	}
	if log.dailyRolling || log.sizeRolling {
		go log.fileCheck()
	}

	return log, nil
}

func (logg *Logger) setLogLevel(_level string) {
	switch _level {
	case "ALL":
		logg.logLevel = 0
	case "DEBUG":
		logg.logLevel = 1
	case "INFO":
		logg.logLevel = 2
	case "WARN":
		logg.logLevel = 3
	case "ERROR":
		logg.logLevel = 4
	case "FATAL":
		logg.logLevel = 5
	case "OFF":
		logg.logLevel = 6
	default:
		logg.logLevel = 0
	}
}

func catchError() {
	if err := recover(); err != nil {
		log.Println("err", err)
	}
}

func (logg *Logger) fileCheck() {
	timerTask := time.Tick(time.Second)

	for {
		select {
		case now := <-timerTask:
			rename := false
			file, err := logg.logObj.logfile.Stat()

			if file == nil || err != nil && os.IsNotExist(err) {
				logg.createFile(false)
				continue
			}

			if logg.sizeRolling {
				if file.Size() >= logg.maxFileSize {
					rename = true
				}
			}

			if !rename && logg.dailyRolling {
				nowDate, _ := time.Parse(DATEFORMAT, now.Format(DATEFORMAT))
				nextDate, _ := time.Parse(DATEFORMAT, now.Add(time.Second).Format(DATEFORMAT))
				if nowDate.Before(nextDate) {
					rename = true
				}
			}

			if rename {
				err1 := logg.createFile(true)
				err2 := logg.deleteFile()

				if err1 != nil {
					logg.Error(err1.Error())
				}
				if err2 != nil {
					logg.Error(err2.Error())
				}
			}
		}
	}
}

func mkdirlog(dir string) error {
	_, err := os.Stat(dir)
	b := err == nil || os.IsExist(err)
	if b {
		return nil
	}
	err = os.MkdirAll(dir, 0776)
	return err
}

func (logg *Logger) createFile(needRename bool) error {
	defer catchError()
	var err error
	obj := logg.logObj

	if err = mkdirlog(obj.dir); err != nil {
		return err
	}

	if needRename {
		fn := obj.dir + "/" + obj.filename + "." + time.Now().Format(TIMEFORMAT)
		if err = os.Rename(obj.dir+"/"+obj.filename, fn); err != nil {
			logg.Error(obj.filename, " rename error: ", err.Error())
		}
	}

	var nextLogfile *os.File
	if nextLogfile, err = os.OpenFile(obj.dir+"/"+obj.filename, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666); err != nil {
		logg.Error("create file error. filename: ", obj.filename)
		return err
	}

	if obj.logfile != nil {
		defer obj.mu.Unlock()
		obj.mu.Lock()
		obj.logfile.Close()
	}

	obj.logfile = nextLogfile

	if logg.withPrefix {
		obj.lg = log.New(obj.logfile, "", log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		obj.lg = log.New(obj.logfile, "", 0)
	}

	return nil
}

func (logg *Logger) deleteFile() error {
	defer catchError()
	dirList, err := ioutil.ReadDir(logg.logObj.dir)
	if err != nil {
		return err
	}
	hadFile := int32(0)
	for i := len(dirList) - 1; i >= 0; i-- {
		if strings.Index(dirList[i].Name(), logg.logObj.filename+".") == 0 && !dirList[i].IsDir() {
			hadFile++
			//fmt.Println(dirList[i].Name(), hadFile)
			if hadFile > logg.maxFileCount {
				if err = os.Remove(logg.logObj.dir + "/" + dirList[i].Name()); err != nil {
					logg.Error("remove file error. fileName:", dirList[i].Name(), ". error: ", err.Error())
					return err
				}
			}
		}
	}
	return nil
}

func (logg *Logger) console(level string, s ...interface{}) {
	if logg.withPrefix == false {
		log.Println(s)
		return
	}

	funcName := "???"
	pc, file, line, ok := runtime.Caller(2)
	if !ok {
		file = "???"
		line = 0
	} else {
		funcName = runtime.FuncForPC(pc).Name()
	}

	short := file
	for i := len(file) - 1; i > 0; i-- {
		if file[i] == '/' {
			short = file[i+1:]
			break
		}
	}

	log.Println(short, line, funcName, level, s)
}

func (logg *Logger) Log(v ...interface{}) (err error) {
	if logg.fileAppender {
		defer func() {
			r := recover()
			if r != nil {
				err = errors.New(r.(string))
			}
		}()

		if logg.logObj != nil {
			logg.logObj.lg.Output(2, fmt.Sprintln(v))
		}
	}

	if logg.consoleAppender {
		logg.console("", v)
	}
	return
}

func getFuncName() string {
	pc, _, _, ok := runtime.Caller(2)
	if ok {
		return runtime.FuncForPC(pc).Name()
	}

	return "???"
}
func (logg *Logger) Debug(v ...interface{}) {
	if logg.logLevel > DEBUG {
		return
	}

	if logg.fileAppender {
		defer catchError()

		if logg.logObj != nil {
			logg.logObj.lg.Output(2, fmt.Sprintln(getFuncName(), "debug", v))
		}
	}

	if logg.consoleAppender {
		logg.console("debug", v...)
	}
}

func (logg *Logger) Info(v ...interface{}) {
	if logg.logLevel > INFO {
		return
	}
	if logg.fileAppender {
		defer catchError()

		if logg.logObj != nil {
			logg.logObj.lg.Output(2, fmt.Sprintln(getFuncName(), "info", v))
		}
	}

	if logg.consoleAppender {
		logg.console("info", v...)
	}
}

func (logg *Logger) Warn(v ...interface{}) {
	if logg.logLevel > WARN {
		return
	}
	if logg.fileAppender {
		defer catchError()

		if logg.logObj != nil {
			logg.logObj.lg.Output(2, fmt.Sprintln(getFuncName(), "warn", v))
		}
	}

	if logg.consoleAppender {
		logg.console("warn", v...)
	}
}

func (logg *Logger) Error(v ...interface{}) {
	if logg.logLevel > ERROR {
		return
	}
	if logg.fileAppender {
		defer catchError()

		if logg.logObj != nil {
			logg.logObj.lg.Output(2, fmt.Sprintln(getFuncName(), "error", v))
		}
	}

	if logg.consoleAppender {
		logg.console("error", v...)
	}
}

func (logg *Logger) Fatal(v ...interface{}) {
	if logg.logLevel > FATAL {
		return
	}
	if logg.fileAppender {
		defer catchError()

		if logg.logObj != nil {
			logg.logObj.lg.Output(2, fmt.Sprintln(getFuncName(), "fatal", v))
		}
	}

	if logg.consoleAppender {
		logg.console("fatal", v...)
	}
}
