package ysoss

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"os"
	"strings"
	"time"
	"ys4k_gin/common"
	"ys4k_gin/logger"
	"ys4k_gin/mtsreq"
	"ys4k_gin/ysdb"
)

var (
	ylog *logger.Logger
	db   *gorm.DB
)

const (
	PART_SIZE = 15 * 1024 * 1024
)

func Init() {
	logger, err := logger.NewLogger(&logger.LoggerConf{
		Dir:          "../logs",
		Filename:     common.YS_LOG_OSS,
		DailyRolling: true,
		SizeRolling:  true,
		MaxFileSize:  50 * logger.MB,
		MaxFileCount: 10,
		Withprefix:   true,
	})
	if err != nil {
		fmt.Printf("NewLogger error: ", err)
		os.Exit(1)
	}

	ylog = logger

	db = ysdb.DB

}

//multipart upload local file, using oss-sdk.
func MultiUploadSimple(localPath, bucketName, obj string) (e error) {
	bucket, e := newBucket(bucketName)
	if e != nil {
		ylog.Error("get bucket error:", e)
		return
	}

	fileMd5, e := getFileMd5(localPath)
	if e != nil {
		ylog.Error("get fileMd5 error:", e)
		return
	}

	//if the file is uploading or uploaded.
	var u ysdb.MultiUpload
	if !db.Where("md5 = ? AND state in (?, ?, ?)", fileMd5, 0, 1, 2).First(&u).RecordNotFound() {
		ylog.Info("file already in multiUpload process or completed upload")
		return errors.New("reduplicated upload")
	}

	bucket.UploadFile()
	chunks, e := oss.SplitFileByPartSize(localPath, PART_SIZE)
	if e != nil {
		ylog.Error("split multi part error :", e)
		return
	}
	//init a multiUpload event
	imur, e := bucket.InitiateMultipartUpload(obj)
	if e != nil {
		ylog.Error("init multipart upload event error:", e)
		return
	}
	upload := ysdb.MultiUpload{
		UploadId: imur.UploadID,
		Md5:      fileMd5,
		State:    ysdb.UPLOADED_TO_SERVER,
		Bucket:   bucketName,
		Object:   obj,
		Tm:       time.Now().Unix(),
	}
	if e = db.Create(&upload).Error; e != nil {
		ylog.Error("create record error:", e)
		AbortMultiUpload(imur, bucket)
		return
	}

	sqlStr := "INSERT INTO upload_part(upload_id, part, state ) VALUES "
	vals := []interface{}{}
	const rowSQL = "(?, ?, ?)"
	var inserts []string

	for i := 0; i < len(chunks); i++ {
		inserts = append(inserts, rowSQL)
		vals = append(vals, imur.UploadID, i+1, ysdb.UPLOAD_PART_NOT_IN_OSS)
	}
	sqlStr = sqlStr + strings.Join(inserts, ",")

	if e := db.Exec(sqlStr, vals...).Error; e != nil {
		ylog.Error("insert uploads error:", e)
		AbortMultiUpload(imur, bucket)
		return e
	}

	go doMultiUpload(localPath, &imur, bucket, &chunks)

	return
}

func MultiUploadPercent(uploadID string) (percent int) {
	var cpn int
	var all int
	if e := db.Where(&ysdb.MultiUploadPart{UploadId: uploadID, State: ysdb.UPLOAD_PART_IN_OSS}).Count(&cpn).Error; e != nil {
		ylog.Error("query error1:", e)
		return
	}
	if e := db.Where(&ysdb.MultiUploadPart{UploadId: uploadID}).Count(&all).Error; e != nil {
		ylog.Error("query error2:", e)
		return
	}

	return cpn * 100 / all

}

func AbortMultiUpload(imur oss.InitiateMultipartUploadResult, bucket *oss.Bucket) (e error) {
	if e = bucket.AbortMultipartUpload(imur); e != nil {
		ylog.Error("abort multiUpload error:", e)
		return
	}
	if e = db.Model(&ysdb.MultiUpload{}).Where("upload_id = ? ", imur.UploadID).
		Update("state", ysdb.UPLOAD_CANCELLED).Error; e != nil {
		ylog.Error("abort multiUpload updating db error:", e)
		return
	}

	return
}

/*********************************************************/

func doMultiUpload(locaFilename string, imur *oss.InitiateMultipartUploadResult, bucket *oss.Bucket, chunks *[]oss.FileChunk) (e error) {

	fd, e := os.Open(locaFilename)
	if e != nil {
		ylog.Error("open file error:", e)
		return
	}
	defer fd.Close()
	// upload part
	var parts []oss.UploadPart
	for _, chunk := range *chunks {
		fd.Seek(chunk.Offset, os.SEEK_SET)
		part, e := bucket.UploadPart(*imur, fd, chunk.Size, chunk.Number)
		if e != nil {
			ylog.Error("UploadPart Error:", e)
			return e
		}

		parts = append(parts, part)

		//update part's state and md5 in db-
		if e := db.Model(&ysdb.MultiUploadPart{}).Where("upload_id=? AND part = ?", imur.UploadID, part.PartNumber).
			Update(map[string]interface{}{"state": ysdb.UPLOAD_PART_IN_OSS, "md5": part.ETag[1 : len(part.ETag)-1]}).Error; e != nil {
			ylog.Error("update uploadPart state error:", e)
			return e
		}
	}
	// complete multipart upload
	cmur, e := bucket.CompleteMultipartUpload(*imur, parts)
	if e != nil {
		ylog.Error("UploadPart Error:", e)
		return
	}
	if e = db.Model(&ysdb.MultiUpload{}).Where("upload_id = ?", imur.UploadID).Update("state", ysdb.UPLOADED_TO_OSS).Error; e != nil {
		ylog.Error("complete multiUpload update state error:", e)
		return
	}
	ylog.Info("CompleteMultipartUploadResult:", cmur)
	return
}

func newClient() (*oss.Client, error) {
	//return oss.New("oss-cn-beijing.aliyuncs.com", mtsreq.ACCESS_KEY_ID, mtsreq.ACCESS_KEY_SECRET)
	return oss.New("oss-cn-beijing-internal.aliyuncs.com", mtsreq.ACCESS_KEY_ID, mtsreq.ACCESS_KEY_SECRET)
}

func newBucket(bucketName string) (*oss.Bucket, error) {
	client, e := newClient()
	if e != nil {
		ylog.Error("new oss client error:", e)
		return nil, e
	}

	bucket, e := client.Bucket(bucketName)
	if e != nil {
		ylog.Error("get bucket error:", e)
		return nil, e
	}
	return bucket, nil
}

func getFileMd5(localPath string) (fileMd5 string, e error) {
	fileInfo, e := os.Stat(localPath)

	if os.IsNotExist(e) {
		return
	}

	h := md5.New()
	fmt.Println("file size:", fileInfo.Size(), fileInfo.Size() < 1024*2)

	if fileInfo.Size() < 1024*1024*1 {
		bys, e := ioutil.ReadFile(localPath)
		if e != nil {
			return "", e
		}
		h.Write(bys)
		return hex.EncodeToString(h.Sum(nil)), nil
	}

	f, _ := os.OpenFile(localPath, os.O_RDONLY, 0666)

	defer f.Close()

	buf := make([]byte, 1024)
	_, e = f.ReadAt(buf, 0)

	h.Write(buf)

	_, e = f.ReadAt(buf, fileInfo.Size()/2)
	h.Write(buf)

	_, e = f.ReadAt(buf, fileInfo.Size()-1024)
	h.Write(buf)

	return hex.EncodeToString(h.Sum(nil)), nil

}
