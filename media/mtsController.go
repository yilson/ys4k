package media

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"os"

	"strconv"
	"ys4k_gin/common"
	"ys4k_gin/logger"
	"ys4k_gin/mtsreq"
	"ys4k_gin/mtsresp"
	"ys4k_gin/ysdb"
	"ys4k_gin/ysoss"
)

var (
	ylog *logger.Logger
	db   *gorm.DB
)

const (
	RESPONSE_CODE_NOT_OK = 0
	RESPONSE_CODE_OK     = 1
)

func Init() {
	logger, err := logger.NewLogger(&logger.LoggerConf{
		Dir:          "../logs",
		Filename:     common.YS_LOG_MTS_CONTROLLER,
		DailyRolling: true,
		SizeRolling:  true,
		MaxFileSize:  50 * logger.MB,
		MaxFileCount: 10,
		Withprefix:   true,
	})
	if err != nil {
		fmt.Printf("NewLogger error: ", err)
		os.Exit(1)
	}

	ylog = logger

	db = ysdb.DB

}

/*
提交转码任务- POST

Params:
	{
		"bucket":"yunshang",
		"location":"oss-cn-beijing",
		"object":"demo/4k/mts-out/TotalRecall.mp4"
	}
*/
func SubmitTranscoding(c *gin.Context) {
	var sp submitJobParams
	if e := c.ShouldBindJSON(&sp); e != nil {
		ylog.Error("bindJsonParam_error", e)
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
		return
	}

	input := mtsreq.InputFile{
		Bucket:   sp.Bucket,
		Location: sp.Location,
		Object:   sp.Object[1:],
	}
	outs := []mtsreq.Output{{
		//OutputObject: "demo/4k/mts-out/test/" + common.GetFileNameNoExt(sp.Object) + ".mp4",
		//todo 4k输出bucket待定
		OutputObject: "mts_out/" + common.GetFileNameNoExt(sp.Object) + ".mp4",
		TemplateId:   TRANSCODING_TEMPLATE_264_0_1K,
	}}
	inputBytes, _ := json.Marshal(input)
	outsBytes, _ := json.Marshal(outs)
	req := mtsreq.SubmitJobsReq{
		CommonParam: mtsreq.NewCommonParam(),
		Action:      "SubmitJobs",
		Input:       string(inputBytes),
		//todo 4k输出bucket待定
		OutputBucket: "yunshangvideo",
		//OutputBucket:   "yunshangvideo",
		OutputLocation: "oss-cn-beijing",
		Outputs:        string(outsBytes),
		PipelineId:     common.MTS_PIPELINE_ONE_ID,
	}
	//向阿里云媒体处理服务提交转码作业
	respBytes, e := req.SendGet()
	if e != nil {
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
		return
	}

	//ylog.Info("QueryJobList_resp:", string(respBytes))

	var resp mtsresp.SubmitJobsResp
	if e := json.Unmarshal(respBytes, &resp); e != nil {
		ylog.Error("SubmitTranscoding_Unmarshal_error", e)
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
		return
	}

	if len(resp.JobResultList.JobResult) > 0 {
		//提交转码作业成功，记录video转码任务到db
		job := ysdb.TransJob{
			JobId:      resp.JobResultList.JobResult[0].Job.JobId,
			VideoId:    sp.VideoId,
			TemplateId: TRANSCODING_TEMPLATE_264_0_1K,
			Obj:        sp.Object,
			State:      ysdb.DB_TRANS_JOB_STATE_SUBMIT_SUCCESS,
			Exception:  "",
			Username:   sp.Username,
			Location:   sp.Location,
			Bucket:     sp.Bucket,
		}
		if e := db.Create(&job).Error; e != nil {
			ylog.Info("SubmitTranscoding_db create error:", e, "| videoId:", sp.VideoId)
			c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
			return
		}
		ylog.Info("SubmitTranscoding_resp--jobId:", resp.JobResultList.JobResult[0].Job.JobId, " | videoId", sp.VideoId)
		c.JSON(http.StatusOK, gin.H{"code": RESPONSE_CODE_OK})
	} else {
		ylog.Info("SubmitTranscoding_resp--failed:", "videoId:", sp.VideoId, " | ", resp.Code)
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
	}
	return
}
func TestSubmitTranscoding(c *gin.Context) {
	var sp submitJobParams
	if e := c.ShouldBindJSON(&sp); e != nil {
		ylog.Error("bindJsonParam_error", e)
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
		return
	}

	//提交转码作业成功，记录video转码任务到db
	job := ysdb.TransJob{
		JobId:     common.RandStringBytesMaskImpr(30),
		VideoId:   sp.VideoId,
		Obj:       sp.Object,
		State:     ysdb.DB_TRANS_JOB_STATE_SUBMIT_SUCCESS,
		Exception: "",
		Username:  sp.Username,
		Location:  sp.Location,
		Bucket:    sp.Bucket,
	}
	if e := db.Create(&job).Error; e != nil {
		ylog.Info("SubmitTranscoding_db create error:", e, "| videoId:", sp.VideoId)
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
		return
	}
	ylog.Info("SubmitTranscoding_resp--jobId:", " | videoId", sp.VideoId)
	c.JSON(http.StatusOK, gin.H{"code": RESPONSE_CODE_OK})

	return
}

/*
查询转码任务
*/
func QueryJobList(c *gin.Context) {
	videoId, e := strconv.ParseInt(c.Query("videoId"), 10, 64)
	if e != nil {
		ylog.Error("parse videoId error:", e)
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
		return
	}
	//获取videoId对应的jobId
	var job ysdb.TransJob
	if e := db.Where("video_id = ?  ", videoId).First(&job).Error; e != nil {
		ylog.Error("QueryJobList--get jobId by VideoId from db--error:", e, "|videoId:", videoId)
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
		return
	}
	ylog.Info("get-jobId-by-videoId,job:", job)

	if job.VideoId > 0 {
		c.JSON(http.StatusOK, gin.H{"code": RESPONSE_CODE_OK, "result": videoTransState{
			VideoId:    videoId,
			Percent:    job.Percent,
			TransState: job.State,
		}})
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
	}
	return
}

/*
cancel transcode job
*/
func CancelTransCodeJob(c *gin.Context) {
	videoId, e := strconv.ParseInt(c.Query("videoId"), 10, 64)
	if e != nil {
		ylog.Error("parse videoId error:", e)
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
		return
	}
	//get jobId by videoId
	var job ysdb.TransJob
	if e := db.Where("video_id = ?  ", videoId).First(&job).Error; e != nil {
		ylog.Error("QueryJobList--get jobId by VideoId from db--error:", e, "|videoId:", videoId)
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
		return
	}
	ylog.Info("get-jobId-by-videoId,job:", job)

	if job.VideoId > 0 {
		//pause pipeline
		updatePipeReq := mtsreq.UpdatePipelineReq{
			CommonParam: mtsreq.NewCommonParam(),
			Action:      "UpdatePipeline",
			PipelineId:  common.MTS_PIPELINE_ONE_ID,
			Name:        common.MTS_PIPELINE_ONE_NAME,
			State:       common.MTS_PIPELINE_PAUSED,
		}
		if _, e := updatePipeReq.SendGet(); e != nil {
			ylog.Error("pause pipeline error", e)
			c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
			return
		}
		//cancel job
		cancelReq := mtsreq.CancelJobReq{
			CommonParam: mtsreq.NewCommonParam(),
			Action:      "CancelJob",
			JobId:       job.JobId,
		}
		resp, e := cancelReq.SendGetHandleResp()
		if e != nil {
			c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
			return
		}

		if resp.Code != "" {
			ylog.Error("cancel transcode job error, code:", resp.Code)
			c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
			return
		}

		//active pipeline
		updatePipeReq.State = common.MTS_PIPELINE_ACTIVE
		for i := 0; i < 5; i++ {
			updateResp, e := updatePipeReq.SendGetHandleResp()
			if e != nil {
				ylog.Error("active pipeline error:", e, " |", i)
				c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
				continue
			}
			if updateResp.Pipeline.State == common.MTS_PIPELINE_ACTIVE {
				break
			}
		}
		c.JSON(http.StatusOK, gin.H{"code": RESPONSE_CODE_OK})
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})
	}
}

/*
multipart upload from local
*/
func MultiUploadVideoFromLocal(c *gin.Context) {
	localPath := c.Param("file")
	if e := ysoss.MultiUploadSimple(
		localPath,
		ysoss.BUCKET_YUNSHANG_VIDEO,
		ysoss.OBJ_PREFIX_MTS_IN+common.GetFileNameNoExt(localPath)); e != nil {
		ylog.Error("multiUpload error:", e)
		c.JSON(http.StatusOK, gin.H{"code": RESPONSE_CODE_OK})
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"code": RESPONSE_CODE_NOT_OK})

	return
}

/**************************************************************************************************************
**************************************************************************************************************/
