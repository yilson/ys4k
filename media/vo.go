package media

const (
	//转码模板id
	TRANSCODING_TEMPLATE_264_30_15K = "72baf8be96886f19903961c7bcb0c129"
	TRANSCODING_TEMPLATE_264_0_1K   = "8a5c39ebc1e84442b7567dd130c592fa"
)

//查询转码任务状态 返回值封装struct
type videoTransState struct {
	VideoId    int64 `json:"videoId"`
	Percent    int   `json:"percent"`
	TransState int   `json:"transState"`
}

//提交转码任务 参数封装struct
type submitJobParams struct {
	Bucket   string `json:"bucket"`
	Location string `json:"location"`
	Object   string `json:"object"`
	Username string `json:"username"`
	VideoId  int64  `json:"video_id"`
}
