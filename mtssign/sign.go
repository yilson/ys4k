package mtssign

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
)

//HMAC-SHA1加密
func SHA1(plaintext, secret string) string {
	signer := hmac.New(sha1.New, []byte(secret+"&"))
	signer.Write([]byte(plaintext))
	hex := signer.Sum(nil)
	return base64.StdEncoding.EncodeToString(hex)
}
