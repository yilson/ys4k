package mts_test

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/thoas/go-funk"
	"testing"
	"time"
	"ys4k_gin/ysdb"
	"ys4k_gin/ysoss"
)

func TestTimestamp(t *testing.T) {
	var jsonWhiteList []string = []string{"Signature", "Timestamp", "Input", "Output", "Outputs"}
	fmt.Println(funk.ContainsString(jsonWhiteList, "Timestamp"))
}

func TestChan(t *testing.T) {
	c1 := make(chan int, 1)

	c1 <- 1
	fmt.Println("a", <-c1)
}

func TestDb(t *testing.T) {

	fmt.Println("DB open")
	db, err := gorm.Open("mysql", "root:Yunshang_sql2014@tcp(47.94.135.22:3306)/ys_mts?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		fmt.Println("gorm connect mysql error:", err)
		panic(db)
	}
	fmt.Println("DB connection successes.")

	db.SingularTable(true)

	upload := ysdb.MultiUpload{
		UploadId: "aa",
	}
	db.Model(&upload).Where("part = ?", 2).Update("state", 1)

}

func TestMultiUpload(t *testing.T) {
	ysdb.Init()
	ysoss.Init()
	if e := ysoss.MultiUploadSimple("C:\\ys\\new.mp4", "yunshangvideo", "mts_in/new1.mp4"); e != nil {
		fmt.Println("eeeeeeeeeeeeeee:", e)
		return
	}
	time.Sleep(time.Hour)
}
