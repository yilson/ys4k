package mtsresp

//更新管道返回
type UpdatePipelineResp struct {
	CommonResp
	Pipeline Pipeline
}

/*
"Pipeline":{
            "Id":"31fa3c9ca8134f9cec2b4b0b0f787830",
            "Name":"qupai-pipeline",
            "State":"Active",
            "Speed":"Standard",
            "NotifyConfig":{
                "Topic":"mts-topic-1"
            }，
            "Role":"AliyunMTSDefaultRole"
        }
*/
type Pipeline struct {
	Id           string
	Name         string
	State        string
	Speed        string
	NotifyConfig NotifyConfig
	Role         string
}
type NotifyConfig struct {
	Topic string
}
