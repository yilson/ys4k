package mtsresp

type MediaInfoResp struct {
	CommonResp
	MediaInfoJob MediaInfoJob `json:"MediaInfoJob"`
}

type MediaInfoJob struct {
	JobId      string
	Input      InputFile
	Properties Properties
}

type Properties struct {
	Streams  Streams
	Duration string //时长 eg："9701.696000"
	Width    string //eg:1920
	Height   string //eg:1080
	FileSize string //大小 B
	Fps      string // 帧率"Fps":"23.976025"
}

type Streams struct {
	VideoStreamList VideoStreamList
}
type VideoStreamList struct {
	VideoStream []VideoStream
}
type VideoStream struct {
	CodecName string
	Bitrate   string //码率
}
