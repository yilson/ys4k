package mtsresp

type CommonResp struct {
	RequestId string

	//以下字段，请求出错时返回
	HostId  string `json:"HostId,omitempty"`
	Code    string `json:"Code,omitempty"`
	Message string `json:"Message,omitempty"`
}

const (
	TRANS_JOB_STATE_SUBMITTED   = "Submitted"
	TRANS_JOB_STATE_TRANSCODING = "Transcoding"
	TRANS_JOB_STATE_SUCCESSED   = "TranscodeSuccess"
)
