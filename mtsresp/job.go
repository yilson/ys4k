/*
转码作业相关接口返回值
*/
package mtsresp

/*
提交转码作业返回
JobResultList	AliyunJobResult[ ]	提交转码作业结果列表
*/
type SubmitJobsResp struct {
	CommonResp
	JobResultList JobResultList
}

/*
查询转码作业返回
*/
type QueryJobListResp struct {
	CommonResp
	JobList JobList
} /*
取消转码作业返回
*/
type CancelJobResp struct {
	CommonResp
	JobId string
}

type JobResultList struct {
	JobResult []JobResult
}
type JobResult struct {
	Success bool
	Code    string
	Message string
	Job     Job
}

type JobList struct {
	Job []Job
}

type Job struct {
	JobId        string
	Input        InputFile
	Output       Output
	State        string
	Code         string
	Message      string
	Percent      int
	PipelineId   string
	CreationTime string
}

type Output struct {
	OutputFile    OutputFile
	TemplateId    string
	WaterMarkList WaterMarkList
	UserData      string
}
type WaterMarkList struct {
	WaterMark []WaterMark
}

type WaterMark struct {
	WaterMarkTemplateId string    `json:"WaterMarkTemplateId,omitempty"`
	InputFile           InputFile `json:"InputFile,omitempty"`
}
type OutputFile struct {
	Bucket   string
	Location string
	Object   string
}

type InputFile struct {
	Bucket   string `json:"Bucket,omitempty"`
	Location string `json:"Location,omitempty"`
	Object   string `json:"Object,omitempty"`
}
