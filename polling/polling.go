package polling

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"net/http"
	"os"
	"strings"
	"time"
	"ys4k_gin/common"
	"ys4k_gin/logger"
	"ys4k_gin/mtsreq"
	"ys4k_gin/mtsresp"
	"ys4k_gin/ysdb"
)

var (
	ylog *logger.Logger
	db   *gorm.DB
)

func Init() {
	logger, err := logger.NewLogger(&logger.LoggerConf{
		Dir:          "../logs",
		Filename:     common.YS_LOG_POLLING,
		DailyRolling: true,
		SizeRolling:  true,
		MaxFileSize:  50 * logger.MB,
		MaxFileCount: 10,
		Withprefix:   true,
	})
	if err != nil {
		fmt.Printf("NewLogger error: ", err)
		os.Exit(1)
	}

	ylog = logger

	db = ysdb.DB

	go pollingTransJobState()
}

func pollingTransJobState() {
	count := 0
	for {
		if count > 0 {
			time.Sleep(time.Second * 10)
		}
		count++
		var jobs []ysdb.TransJob
		if e := db.Where("state = ? ", ysdb.DB_TRANS_JOB_STATE_SUBMIT_SUCCESS).Find(&jobs).Error; e != nil {
			ylog.Error("query jobIds error", e)
			continue
		}

		ylog.Info("jobs to check from aliyun_mts:", jobs)
		//阿里云查询转码任务状态，每次最多10个
		for i := 0; i < len(jobs); i += 10 {
			var jobIds []string
			edge := i
			if i+10 > len(jobs) {
				edge = len(jobs)
			}
			for _, job := range jobs[i:edge] {
				ylog.Info("Job-video: ", job.JobId, " | ", job.VideoId)
				jobIds = append(jobIds, job.JobId)
			}
			req := mtsreq.QueryJobListReq{
				CommonParam: mtsreq.NewCommonParam(),
				Action:      "QueryJobList",
				JobIds:      strings.Join(jobIds, ","),
			}
			respBytes, e := req.SendGet()
			if e != nil {
				ylog.Error("sendGet_error:", e, "|ids:", jobIds)
				continue
			}

			ylog.Info("QueryJobList_resp:", string(respBytes))

			var resp mtsresp.QueryJobListResp
			if e := json.Unmarshal(respBytes, &resp); e != nil {
				ylog.Error("Unmarshal_error", e)
				continue
			}

			if len(resp.JobList.Job) > 0 {
				//根据返回状态，修改转码记录状态
				for _, job := range resp.JobList.Job {
					ylog.Info("resp.JobList.Job :", job)
					var tj ysdb.TransJob
					m := make(map[string]interface{})
					if job.State == mtsresp.TRANS_JOB_STATE_SUCCESSED {
						tj = ysdb.TransJob{State: ysdb.DB_TRANS_JOB_STATE_TRANSCODING_SUCCESS, Percent: job.Percent}
						m["uri"] = "/" + job.Output.OutputFile.Object
						m["transcode"] = ysdb.DB_TRANS_JOB_STATE_TRANSCODING_SUCCESS
						m["transPercent"] = job.Percent
					} else if job.State == mtsresp.TRANS_JOB_STATE_SUBMITTED || job.State == mtsresp.TRANS_JOB_STATE_TRANSCODING {
						tj = ysdb.TransJob{Percent: job.Percent}
						m["transcode"] = ysdb.DB_TRANS_JOB_STATE_SUBMIT_SUCCESS
						m["transPercent"] = job.Percent
					} else {
						tj = ysdb.TransJob{State: ysdb.DB_TRANS_JOB_STATE_EXCEPTION, Exception: job.State}
						m["transcode"] = ysdb.DB_TRANS_JOB_STATE_EXCEPTION
						m["transPercent"] = job.Percent
					}
					if e := db.Model(&ysdb.TransJob{}).Where("job_id = ?", job.JobId).Update(tj).Error; e != nil {
						ylog.Error("update TransJob error: ", e)
						continue
					}

					//若转码成功，通知cms和推送系统
					if job.State == mtsresp.TRANS_JOB_STATE_SUCCESSED {
						//通知推送系统
						videoUrl := "http://" + job.Output.OutputFile.Bucket + "." + job.Output.OutputFile.Location + ".aliyuncs.com/" + job.Output.OutputFile.Object
						_, e := http.Get("http://116.62.204.54:9521/start/channel?user=demo&type=CDN&url=" + videoUrl)
						if e != nil {
							ylog.Error("notify push GET error:", e)
							return
						}

						/*bysPush, _ := ioutil.ReadAll(respPush.Body)
						defer respPush.Body.Close()
						ylog.Info("Push response: ", string(bysPush))*/
					}

					//notify video_service to update uri/transcode/transpervcent
					bys, e := json.Marshal(m)
					if e != nil {
						ylog.Error("marshal params_modifyVideoInfo error:", e)
						continue
					}
					var videoId int64
					var username string
					for _, dbJob := range jobs[i:edge] {
						if dbJob.JobId == job.JobId {
							videoId = dbJob.VideoId
							username = dbJob.Username
						}
					}

					req, e := http.NewRequest("POST", fmt.Sprintf("http://video.cloutropy.com/video/info/%d", videoId), bytes.NewReader(bys))
					if e != nil {
						ylog.Error("newRequest to modifyVideoInfo error:", e)
						continue
					}
					req.Header.Set("Content-Type", "application/json;charset=UTF-8")
					req.AddCookie(&http.Cookie{Name: "USERNAME", Value: username})

					ylog.Info("ready to notify video_service to ", job.JobId, jobs[i].VideoId)
					client := http.Client{}
					if _, e := client.Do(req); e != nil {
						ylog.Error("modifyVideoInfo_client_do error:", e)
						continue
					}
				}
			}
		}

	}
}
