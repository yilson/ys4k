package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"

	"ys4k_gin/common"
	"ys4k_gin/logger"
	"ys4k_gin/media"
	"ys4k_gin/movie"
	"ys4k_gin/mtsreq"
	"ys4k_gin/polling"
	"ys4k_gin/ysdb"
	"ys4k_gin/ysoss"
)

var (
	ylog *logger.Logger
)

func setupRouter() *gin.Engine {
	r := gin.Default()

	r.GET("/video_list", movie.MovieInfos)
	r.POST("/transcode", media.SubmitTranscoding)
	//r.POST("/transcode_test", mts_ctl.TestSubmitTranscoding)
	r.GET("/transcode", media.QueryJobList)          // ?videoId=111
	r.DELETE("/transcode", media.CancelTransCodeJob) // ?videoId=111
	r.POST("/upload", media.MultiUploadVideoFromLocal)

	r.PUT("/test/mu", TestM3U8)

	// Ping test
	r.GET("/ping/", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	// Get user value
	r.PUT("/test", func(c *gin.Context) {

		/*var in mtsreq.InputFile
		c.BindJSON(&in)
		c.String(http.StatusOK, "params:%v %s %v", in, c.ClientIP())*/

	})

	return r
}

func main() {
	r := setupRouter()
	// Listen and Server in 0.0.0.0:8080

	ysdb.Init()
	media.Init()
	mtsreq.Init()
	polling.Init()
	movie.Init()
	ysoss.Init()

	logger, err := logger.NewLogger(&logger.LoggerConf{
		Dir:          "../logs",
		Filename:     common.YS_LOG_MAIN,
		DailyRolling: true,
		SizeRolling:  true,
		MaxFileSize:  50 * logger.MB,
		MaxFileCount: 10,
		Withprefix:   true,
	})

	if err != nil {
		fmt.Printf("NewLogger error: ", err)
		os.Exit(1)
	}
	ylog = logger

	/*if e := ysoss.MultiUploadSimple("/root/bigfile.tar", "yunshangvideo", "mts_in/bigfile.mp4"); e != nil {
		ylog.Error("multiUpload error:", e)
		return
	}*/
	/*	in := mtsreq.InputFile{
			Bucket:   "yunshang",
			Location: "oss-cn-beijing",
			Object:   "demo/4k/Avatar.mkv",
		}
		inputBytes, _ := json.Marshal(in)
		m := mtsreq.MediaInfoReq{
			CommonParam: mtsreq.NewCommonParam(),
			Action:      "SubmitMediaInfoJob",
			Input:       string(inputBytes),
		}

		resp, e := m.SendMediaInfoReq()
		if e != nil {
			fmt.Errorf("multiUpload error:%v", e)
			return
		}
		fmt.Println(resp)
		return*/

	r.Run(":8080")
}

func TestM3U8(c *gin.Context) {

}
