package main

import (
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"math"
	"strconv"
	"strings"
	"time"
	"ys4k_gin/mtsreq"
)

var oldDB *gorm.DB
var newDB *gorm.DB

const (
	OSS_HOST = "yunshangvideo.oss-cn-beijing.aliyuncs.com"

	ACCESS_KEY_ID     = "LTAIcyr52G7olaLH"
	ACCESS_KEY_SECRET = "oGsrCRAAe6a95wqj5dnCoWwaxUy3gJ"
)

func main() {
	InitNewDB()
	InitOldDB()
	ovs := make([]OldVideo, 0, 10)
	if e := oldDB.Where("video_id > ?", 200).Find(&ovs).Error; e != nil {
		fmt.Println("get old videos error:", e)
		return
	}
	fmt.Println("get old videos count:", len(ovs))

	nvs := make([]NewVideo, 0, 1)
	if e := newDB.Find(&nvs).Error; e != nil {
		fmt.Println("get new videos error:", e)
		return
	}
	fmt.Println("get new videos count:", len(nvs))

	for _, ov := range ovs {
		fmt.Println("****** Processing ******", "video_id:", ov.VideoID, "******")
		for _, nv := range nvs {
			if ov.VideoName == nv.VideoName {
				continue
			}
		}
		buck := "yunshangvideo"
		if strings.HasPrefix(ov.Uri, "/mts_out") {
			s := ov.Uri
			ov.Uri = "/dapp" + s[strings.LastIndex(s, "/"):]
		}
		in := mtsreq.InputFile{
			Bucket:   buck,
			Location: "oss-cn-beijing",
			Object:   ov.Uri[1:],
		}
		inputBytes, _ := json.Marshal(in)
		m := mtsreq.MediaInfoReq{
			CommonParam: mtsreq.NewCommonParam(),
			Action:      "SubmitMediaInfoJob",
			Input:       string(inputBytes),
		}

		info, e := m.SendMediaInfoReq()
		if e != nil {
			fmt.Errorf("multiUpload error:%v", e)
			continue
		}
		if info.MediaInfoJob.Properties.FileSize == "" {
			continue
		}
		size, e := strconv.ParseInt(info.MediaInfoJob.Properties.FileSize, 10, 64)
		if e != nil {
			fmt.Println("parseInt error:", e)
			continue
		}
		//>100M
		if size > 1024*1024*100 {
			fmt.Println("Need to be transferred to new database according to its file size:", size)

			dt, _ := strconv.ParseFloat(info.MediaInfoJob.Properties.Duration, 64)
			dtF := int(math.Round(dt))
			width, _ := strconv.ParseInt(info.MediaInfoJob.Properties.Width, 10, 64)
			height, _ := strconv.ParseInt(info.MediaInfoJob.Properties.Height, 10, 64)
			fpst, _ := strconv.ParseFloat(info.MediaInfoJob.Properties.Fps, 64)
			fps := int(math.Round(fpst))
			now := time.Now().Format("2006-01-02 15:04:05")

			nv := NewVideo{
				UserID:      1,
				VideoName:   ov.VideoName,
				Md5:         "",
				HostId:      1,
				Uri:         ov.Uri,
				Cover:       ov.ThumbnailUrl,
				Duration:    dtF,
				Size:        size,
				Width:       int(width),
				Height:      int(height),
				Fps:         int(fps),
				State:       1,
				Description: "",
				PublishTime: now,
				CreateTime:  now,
				UpdateTime:  now,
			}
			if e := newDB.Create(&nv).Error; e != nil {
				fmt.Println("save new video error:", e)
				continue
			}
			fmt.Println("save to new database successfully.")
		}

	}

}

func InitNewDB() {
	db, err := gorm.Open("mysql", "root:Yunshang_sql2014@tcp(47.94.135.22:3306)/ys_mts?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		fmt.Println("gorm connect mysql error1:", err)
		panic(db)
	}
	fmt.Println("newDB connection successes.")

	db.SingularTable(true)

	newDB = db
}

func InitOldDB() {
	db, err := gorm.Open("mysql", "root:root@tcp(47.96.188.88:3306)/test?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		fmt.Println("gorm connect mysql error2:", err)
		panic(db)
	}
	fmt.Println("oldDB connection successes.")

	db.SingularTable(true)

	oldDB = db
}

type OldVideo struct {
	VideoID      uint64   `json:"videoID,omitempty"`
	GroupID      uint64   `json:"groupID,omitempty"`
	UserName     string   `json:"userName,omitempty"`
	UserType     int      `json:"userType,omitempty"`
	CommodityID  uint64   `json:"commodityID,omitempty"`
	VideoName    string   `json:"videoName,omitempty"`
	VideoType    string   `json:"videoType,omitempty"`
	Series       string   `json:"series,omitempty"`
	Code         string   `json:"code,omitempty"`
	Uri          string   `json:"uri,omitempty"`
	PartVideoUrl string   `json:"partVideoUrl,omitempty"`
	Duration     float64  `json:"duration,omitempty"`
	Labels       []string `json:"labels,omitempty"`
	Price        uint64   `json:"price,omitempty"`
	// state enum {1:"upload", 2:"publish", 3:"unshelve", 4:"forbidden"}
	State uint `json:"state,omitempty"`
	// transcode: 1-default(transcoding) 2-transcode success 11-transcode exception
	Transcode       uint   `json:"transcode,omitempty"`
	TransPercent    uint   `json:"transPercent,omitempty"`
	ForbiddenReason string `json:"forbiddenReason,omitempty"`
	IsTop           bool   `json:"isTop,omitempty"`
	ThumbnailUrl    string `json:"thumbnailUrl,omitempty"`
	Description     string `json:"description,omitempty"`
	//CommodityList   []VideoCommodity `json:"commodityList,omitempty"`
	VideoLevel    int    `json:"videoLevel,omitempty"`
	PlayCount     uint64 `json:"playCount,omitempty"`
	FavoriteCount uint64 `json:"favoriteCount,omitempty"`
	ShareCount    uint64 `json:"shareCount,omitempty"`
	CollectCount  uint64 `json:"collectCount,omitempty"`
	PublishTime   int64  `json:"publishTime,omitempty"`
	CreateTime    string `json:"-"`
}

func (OldVideo) TableName() string {
	return "video"
}

type NewVideo struct {
	VideoID     uint64 `json:"videoID,omitempty"`
	UserID      uint   `json:"-"`
	VideoName   string `json:"videoName,omitempty"`
	Md5         string `json:"md5,omitempty"`
	HostId      uint   `json:"-"`
	Uri         string `json:"uri,omitempty"`
	Cover       string `json:"cover,omitempty"`
	Duration    int    `json:"duration,omitempty"`
	Size        int64  `json:"size"`
	Width       int    `json:"width"`
	Height      int    `json:"height"`
	Fps         int    `json:"fps"`
	State       uint   `json:"state,omitempty"` // 状态 1-上传完成 2-发布 3-下架 11-上传取消 12-上传失败
	Description string `json:"description,omitempty"`
	PublishTime string `json:"publishTime,omitempty"`
	CreateTime  string `json:"-"`
	UpdateTime  string `json:"updateTime"`
}

func (NewVideo) TableName() string {
	return "video"
}
