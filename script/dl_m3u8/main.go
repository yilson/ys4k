package main

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

const (
	OSS_ENDPOINT          = "yunshangvideo.oss-cn-beijing.aliyuncs.com"
	BUCKET_YUNSHANG_VIDEO = "yunshangvideo"

	ACCESS_KEY_ID     = "LTAIcyr52G7olaLH"
	ACCESS_KEY_SECRET = "oGsrCRAAe6a95wqj5dnCoWwaxUy3gJ"

	M3U8_OSS_OBJ_PREFIX = "mts_out/m3u8test/"
)

var M3u8AddressList []string = []string{"https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8"}

var prefix string

func main() {
	//upload master
	list := dlAndUpMaster(M3u8AddressList[0])
	dlAndUpSubs(list)

	time.Sleep(time.Hour * 24)
}
func dlAndUpSubs(addrs []string) {
	for _, addr := range addrs {
		tcPrefix := prefix + addr[0:strings.LastIndex(addr, "/")+1]
		initAddr := M3u8AddressList[0]
		url := initAddr[0:strings.LastIndex(initAddr, "/")+1] + addr
		fmt.Println("processing m3u8_list subs address...", url)
		rpB, e := http.Get(url)
		if e != nil {
			fmt.Println("get addrA error:", e)
			continue
		}
		defer rpB.Body.Close()

		bys, _ := ioutil.ReadAll(rpB.Body)

		filePaths := strings.Split(addr, "/")

		newFile, err := os.Create(filePaths[1])
		if err != nil {
			fmt.Println("create file error:", err)
			continue
		}

		if _, err := newFile.Write(bys); err != nil {
			fmt.Println("write file error:", e)
			continue
		}

		lines, e := readLines(filePaths[1])
		if e != nil {
			fmt.Println("read lines error:", e)
			continue
		}
		//fmt.Println(lines)

		newFile.Close()

		bucket, _ := newBucket(BUCKET_YUNSHANG_VIDEO)
		fmt.Println("subURI:", prefix+strings.Join(filePaths, "/"), filePaths[1])
		if e := bucket.PutObjectFromFile(prefix+strings.Join(filePaths, "/"), filePaths[1]); e != nil {
			fmt.Println("upload file error:", e)
			continue
		}
		//fmt.Println("upload master done")

		os.Remove(filePaths[1])

		for _, sub := range lines {
			if strings.HasPrefix(sub, "#") {
				continue
			}
			//fmt.Println("tcPrefix:", tcPrefix, tcPrefix+sub)
			url := initAddr[0:strings.LastIndex(initAddr, "/")+1] + addr[0:strings.LastIndex(addr, "/")+1] + sub
			fmt.Println("processing m3u8 tc address...", url)
			rpA, e := http.Get(url)
			if e != nil {
				fmt.Println("get addrA error:", e)
				continue
			}

			filePaths := strings.Split(sub, "/")
			fmt.Println("filePaht:", filePaths[1])

			exist, _ := bucket.IsObjectExist(tcPrefix + strings.Join(filePaths, "/"))
			if exist {
				fmt.Println(tcPrefix+strings.Join(filePaths, "/"), "|| exist, continue")
				rpA.Body.Close()
				continue
			}

			bys, _ := ioutil.ReadAll(rpA.Body)

			rpA.Body.Close()

			newFile, err := os.Create(filePaths[1])
			if err != nil {
				fmt.Println("create file error:", err)
				continue
			}

			if _, err := newFile.Write(bys); err != nil {
				fmt.Println("write file error:", e)
				continue
			}

			//fmt.Println(lines)

			newFile.Close()

			bucket, _ := newBucket(BUCKET_YUNSHANG_VIDEO)
			fmt.Println("tcURI:", tcPrefix+strings.Join(filePaths, "/"))

			if e := bucket.PutObjectFromFile(tcPrefix+strings.Join(filePaths, "/"), filePaths[1]); e != nil {
				fmt.Println("upload file error:", e)
				continue
			}

			os.Remove(filePaths[1])

		}
	}
}

func dlAndUpMaster(addr string) (list []string) {
	fmt.Println("processing m3u8_master address...")
	rpA, e := http.Get(addr)
	if e != nil {
		fmt.Println("get addrA error:", e)
		return
	}
	defer rpA.Body.Close()

	bys, _ := ioutil.ReadAll(rpA.Body)
	/*lines := readLines(bys)
	fmt.Println(lines)*/

	filePath := addr[strings.LastIndex(addr, "/")+1:]

	newFile, err := os.Create(filePath)
	if err != nil {
		fmt.Println("create file error:", e)
		return
	}

	if _, err := newFile.Write(bys); err != nil {
		fmt.Println("write file error:", e)
		return
	}

	lines, e := readLines(filePath)
	if e != nil {
		fmt.Println("read lines error:", e)
		return
	}
	//fmt.Println(lines)

	newFile.Close()

	prefix = M3U8_OSS_OBJ_PREFIX + filePath[0:strings.LastIndex(filePath, ".")] + "/"
	bucket, _ := newBucket(BUCKET_YUNSHANG_VIDEO)
	//fmt.Println(prefix+filePath, filePath)
	if e := bucket.PutObjectFromFile(prefix+filePath, filePath); e != nil {
		fmt.Println("upload file error:", e)
		return
	}
	fmt.Println("upload master done")

	os.Remove(filePath)

	for _, sub := range lines {
		if strings.HasPrefix(sub, "#") {
			continue
		}

		list = append(list, sub)
	}
	return
}

func readLines(path string) (lines []string, err error) {
	var (
		file *os.File
		part []byte
	)

	if file, err = os.Open(path); err != nil {
		fmt.Println("open error:", err, path)
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	buffer := bytes.NewBuffer(make([]byte, 1024))

	i := 0
	for {
		if part, _, err = reader.ReadLine(); err != nil {
			break
		}

		buffer.Write(part)
		if i == 0 {
			i++
			lines = append(lines, "#EXTM3U")
			buffer.Reset()
			continue
		}
		lines = append(lines, buffer.String())
		//fmt.Println("buffffffffffffffff:", buffer.String())
		buffer.Reset()

	}
	if err == io.EOF {
		err = nil
	}
	return
}

func newClient() (*oss.Client, error) {
	return oss.New("oss-cn-beijing.aliyuncs.com", ACCESS_KEY_ID, ACCESS_KEY_SECRET)
	//return oss.New("oss-cn-beijing-internal.aliyuncs.com", ACCESS_KEY_ID, ACCESS_KEY_SECRET)
}

func newBucket(bucketName string) (*oss.Bucket, error) {
	client, e := newClient()
	if e != nil {
		return nil, e
	}

	bucket, e := client.Bucket(bucketName)
	if e != nil {
		return nil, e
	}
	return bucket, nil
}
