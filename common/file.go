package common

import (
	"os"
	"strings"
)

// 判断文件(夹)是否存在
func FileExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

func GetFileName(path string) string {
	ind := strings.LastIndex(path, "/")
	return path[ind+1:]
}

func GetFileNameNoExt(path string) string {
	ind := strings.LastIndex(path, "/")
	return path[ind+1 : strings.LastIndex(path, ".")]
}
