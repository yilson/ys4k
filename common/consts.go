package common

//响应code
const (
	RESPONSE_EXCEPTION = 2
	RESPONSE_OK        = 1
)

const (
	MTS_PIPELINE_ONE_ID   = "02b75d8295614b61a13a4783c239fa07" //转码管道id
	MTS_PIPELINE_ONE_NAME = "mts-service-pipeline"             //转码管道名称
	MTS_PIPELINE_ACTIVE   = "Active"                           //表示管道内的作业会被媒体转码服务调度转码执行；
	MTS_PIPELINE_PAUSED   = "Paused"                           //表示管道暂停，作业不再会被媒体转码调度转码执行，管道内的所有作业状态维持在已提交状态，已经处于转码中的任务将继续转码，不受影响。
)

//各package日志文件名
const (
	YS_LOG_POLLING        = "yspolling.log"
	YS_LOG_MTS_CONTROLLER = "yscontroller.log"
	YS_LOG_MTS_REQUEST    = "ys4kRequest.log"
	YS_LOG_MAIN           = "ys4kmain.log"
	YS_LOG_OSS            = "ysoss.log"
)
