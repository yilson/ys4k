package ysdb

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var DB *gorm.DB

func Init() {
	fmt.Println("DB open")
	db, err := gorm.Open("mysql", "root:Yunshang_sql2014@tcp(47.94.135.22:3306)/ys_mts?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		fmt.Println("gorm connect mysql error:", err)
		panic(db)
	}
	fmt.Println("DB connection successes.")

	db.SingularTable(true)

	DB = db
}
