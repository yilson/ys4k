package ysdb

const (
	//trans_job表state：状态  1-提交成功/转码中 2-转码成功 11-其他
	DB_TRANS_JOB_STATE_SUBMIT_SUCCESS      = 1
	DB_TRANS_JOB_STATE_TRANSCODING_SUCCESS = 2
	DB_TRANS_JOB_STATE_EXCEPTION           = 11

	TRANS_JOB_RESOLUTION_720P  = "720p"
	TRANS_JOB_RESOLUTION_1080P = "1080p"
	TRANS_JOB_RESOLUTION_4K    = "4k"

	UPLOAD_NOT_BEGIN   = 0
	UPLOADED_TO_SERVER = 1
	UPLOADED_TO_OSS    = 2
	UPLOAD_CANCELLED   = 11
	UPLOAD_FAILED      = 12

	UPLOAD_PART_NOT_IN_OSS = 0
	UPLOAD_PART_IN_OSS     = 1
)

//video transcode job table
type TransJob struct {
	JobId      string `json:"-"`          //转码任务id
	VideoId    int64  `json:"-"`          //视频id
	TemplateId string `json:"-"`          //转码模板id
	Resolution string `json:"resolution"` //分辨率 720P 1080P 4K
	Obj        string `json:"-"`          //视频的oss object
	State      int    `json:"state"`      //状态 1-转码中 2-转码成功 11-其他
	Exception  string `json:"-"`          //state为2时，记录阿里云返回的状态
	Username   string `json:"-"`          //上传用户 用户名
	Location   string `json:"-"`          //oss区域：如oss-cn-beijing
	Bucket     string `json:"-"`
	Percent    int    `json:"percent"`
}

//Mulitpart Upload table
type MultiUpload struct {
	UploadId string
	Md5      string //文件MD5
	State    int    //状态 0-未上传 1-已上传至服务器 2-已上传至oss 11-取消上传 12-上传oss失败
	Bucket   string
	Object   string
	Tm       int64
}

//multipart uploadPart table
type MultiUploadPart struct {
	UploadId string
	Part     int
	State    int    //状态 0-未上传至oss 1-已上传至oss
	Md5      string //part MD5
}

func (m *MultiUpload) TableName() string {
	return "upload"
}
func (m *MultiUploadPart) TableName() string {
	return "upload_part"
}
